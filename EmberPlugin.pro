TEMPLATE = subdirs
CONFIG += ordered

include(defaults.pri)

defineReplace(ADDSUBPRO) {
    for(subdirs, 1) {
        FILES = $$files($$subdirs,true)
        RESULT = 
        for(var, $$list($$FILES)) {
            infile($$var, EMBER_PLUGIN) {
#                message (good pro file: $$var)
                RESULT *= $$var
            }
        }
        return($$RESULT)
    }
}

SUBDIRS += $$ADDSUBPRO(./Source/*.pro)

message (EMBER_PLUGIN_SUBDIRS:  $$SUBDIRS)
