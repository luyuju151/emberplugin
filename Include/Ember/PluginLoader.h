﻿#pragma once

#include <string>
#include <vector>
#include "VariationPluginInfo.h"

/// <summary>
/// Hold the loading result of one plugin, containing the variation info and plugin file path/name.
/// </summary>
class LoadResult {
public:
    LoadResult(VariationPluginInfo* i=NULL,const std::wstring & p=L""):info(i),path(p) { }

    VariationPluginInfo* info;
    std::wstring path;
};

/// <summary>
/// Plugin loader holding the load results after calling LoadAllPlugin().
/// </summary>
class PluginLoader {
public:
    std::vector<LoadResult> loadResults;
    
    void LoadAllPlugin();
};


