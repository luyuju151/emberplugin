﻿#pragma once

/// <summary>
/// Info interface from plugins.
/// Hold the size of vector lists and two vectors of Variation<T> *, 
/// pointing the variations generated in the plugin.
/// </summary>
struct VariationPluginInfo
{
    int size=0;
    void* listFloat=0;
    void* listDouble=0;
};


