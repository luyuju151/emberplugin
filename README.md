Ember Plugins for Fractorium
==========

A mini project for building custom variation plugins for fractorium, a Qt-based fractal flame editor which supports rendering in OpenCL.

The project now contains qt `.pro`, headers and several sample plugins.



# Building

## Windows

- Place `ember.lib` in `Deps` (you can use `ember.lib.zip` if it works, otherwise building of Ember of Fractorium is needed)
- Open `EmberPlugin.pro` via Qt Creator to build

## Linux

- Todo



# Install

## Windows

- Copy plugin dll files to `[fractorium folder]/emberplugins`

## Linux / Mac

- Todo



# Developing Variations

- Sample variations are easy to follow and modify

- A ruby script `conv-opencl.rb` can be used to easily convert `Func()` to `OpenCLString()`



