﻿#pragma once
#include "Variation.h"

/// <summary>
/// Macros to create pre and post counterparts to a parametric or parametric variation.
/// Assign type defaults to set.
/// They are slightly different from macros in "Variation.h" because enum VariationId of variations
/// in the plugin are not present there, so that they need to be some dummy ID and
/// then got changed after loading.
/// </summary>
/// 
#define DUMMYVARID eVariationId::LAST_VAR

#define MAKEPREPOSTPLUGINVAR(varFullName, stringName) MAKEPREPOSTPLUGINVARASSIGN(varFullName, stringName, eVariationAssignType::ASSIGNTYPE_SET)
#define MAKEPREPOSTPLUGINVARASSIGN(varFullName, stringName, assignType) \
	template <typename T> \
	class Pre##varFullName : public varFullName<T> \
	{ \
		VARUSINGS \
	public: \
		Pre##varFullName(T weight = 1.0) : varFullName<T>(weight) \
		{ \
			m_VariationId = DUMMYVARID; \
			m_Name = "pre_"#stringName; \
			m_PrePostAssignType = assignType; \
			SetType(); \
		} \
		\
		PREPOSTVARCOPY(Pre##varFullName, varFullName) \
	}; \
	\
	template <typename T> \
	class Post##varFullName : public varFullName<T> \
	{ \
		VARUSINGS \
	public:\
		Post##varFullName(T weight = 1.0) : varFullName<T>(weight) \
		{ \
			m_VariationId = DUMMYVARID; \
			m_Name = "post_"#stringName; \
			m_PrePostAssignType = assignType; \
			SetType(); \
		} \
		\
		PREPOSTVARCOPY(Post##varFullName, varFullName) \
	};


#define MAKEPREPOSTPLUGINPARVAR(varFullName, stringName) MAKEPREPOSTPLUGINPARVARASSIGN(varFullName, stringName, eVariationAssignType::ASSIGNTYPE_SET)
#define MAKEPREPOSTPLUGINPARVARASSIGN(varFullName, stringName, assignType) \
	template <typename T> \
	class Pre##varFullName : public varFullName <T> \
	{ \
		VARUSINGS \
		PARVARUSINGS \
		using varFullName<T>::Init; \
	public:\
		Pre##varFullName(T weight = 1.0) : varFullName<T>(weight) \
		{ \
			m_VariationId = DUMMYVARID; \
			m_Name = "pre_"#stringName; \
			m_PrePostAssignType = assignType; \
			SetType(); \
			Init(); \
		} \
		\
		PREPOSTPARVARCOPY(Pre##varFullName, varFullName) \
	}; \
	\
	template <typename T> \
	class Post##varFullName : public varFullName<T> \
	{ \
		VARUSINGS \
		PARVARUSINGS \
		using varFullName<T>::Init; \
	public:\
		Post##varFullName(T weight = 1.0) : varFullName<T>(weight) \
		{ \
			m_VariationId = DUMMYVARID; \
			m_Name = "post_"#stringName; \
			m_PrePostAssignType = assignType; \
			SetType(); \
			Init(); \
		} \
		\
		PREPOSTPARVARCOPY(Post##varFullName, varFullName) \
	};


/// <summary>
/// Macros to initialize variation list.
/// </summary>
/// 

#define POPULATEVARLIST(varFullName) \
    variationsFloat[i]=new varFullName<float>; \
    variationsDouble[i]=new varFullName<double>; \
    i++; \

#define POPULATEVARLIST_REGPREPOST(varFullName) \
    POPULATEVARLIST(varFullName) \
    POPULATEVARLIST(Pre##varFullName) \
    POPULATEVARLIST(Post##varFullName) \

