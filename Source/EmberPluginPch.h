﻿#ifdef _WIN32
	#pragma once
#endif

/// <summary>
/// Precompiled header file. Place all system includes here with appropriate #defines for different operating systems and compilers.
/// </summary>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN//Exclude rarely-used stuff from Windows headers.
#define _USE_MATH_DEFINES
//#define CL_USE_DEPRECATED_OPENCL_1_2_APIS 1

#define DO_DOUBLE

#if defined(_WIN32)
	#pragma warning(disable : 4251; disable : 4661; disable : 4100)
	#include <windows.h>
//	#include <SDKDDKVer.h>
//	#include "GL/gl.h"
#elif defined(__APPLE__)
	#include <OpenGL/gl.h>
#else
	#include "GL/glx.h"
#endif

#define VARPLUGINEXP __declspec(dllexport)

//#ifdef _WIN32
//	#if defined(BUILDING_EMBERPLUGIN)
//		#define VARPLUGINEXP __declspec(dllexport)
//	#else
//		#define VARPLUGINEXP __declspec(dllimport)
//	#endif
//#else
//	#define VARPLUGINEXP
//#endif


