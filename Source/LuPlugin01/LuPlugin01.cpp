﻿#include "Variation.h"
#include "XForm.h"
#include "VariationPluginInfo.h"
#include "../EmberPluginMacro.h"
#include "../DllExport.h"


namespace EmberNs {

/// <summary>
/// DC Offset.
/// blend with original color with "origin"
/// </summary>
/// varFullName : DCOffset
/// stringName  : dc_offset
template <typename T>
class DCOffset : public ParametricVariation<T>
{
public:
	DCOffset(T weight = 1.0) : ParametricVariation<T>("dc_offset", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCOffset)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T newColor=std::fmod( outPoint.m_ColorX  + mp_offset, T(1)); 
        outPoint.m_ColorX = Lerp( newColor<0 ? newColor+T(1) : newColor ,
                            outPoint.m_ColorX, mp_origin);
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
        }
        else {
            helper.Out.x = m_Weight * helper.In.x;
            helper.Out.y = m_Weight * helper.In.y;
            helper.Out.z = m_Weight * helper.In.z;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_offset = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t newColor=fmod( outPoint->m_ColorX + "<< mp_offset <<", (real_t)(1)); \n"
		      "\t\t outPoint->m_ColorX = Lerp( newColor<0 ? newColor+(real_t)(1) : newColor , \n"
		      "\t\t\t\t\t\t\t outPoint->m_ColorX, "<< mp_origin <<"); \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

//    virtual void Precalc() override {};

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Lerp" };
	}

protected:
	void Init() {
        string prefix = Prefix()+"dc_offset_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_offset, prefix + "offset",0.5));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
    }

private:
	T mp_offset;
	T mp_origin;
};
MAKEPREPOSTPLUGINPARVAR(DCOffset, dc_offset) ;


/// <summary>
/// DC Ring.
/// color=fmod( an*r^n + b*r +offset   ,1) 
/// r is radius from center x,y
/// then crop between in and out
/// blend with original color with "origin"
/// </summary>
/// varFullName : DCRingVariation
/// stringName  : dc_ring
template <typename T>
class DCRingVariation : public ParametricVariation<T>
{
public:
	DCRingVariation(T weight = 1.0) : ParametricVariation<T>("dc_ring", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCRingVariation)


	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T r = std::sqrt( Sqr(helper.In.x-mp_x) + Sqr(helper.In.y-mp_y) );
        if( r >= mp_in && (r <= mp_out || mp_out <0 ) ) {
            if(mp_an !=0 && mp_n != 0) { 
                // color=fmod( scale*(r) +offset   ,1)
                outPoint.m_ColorX = Lerp(std::fmod( mp_an * std::pow( Zeps(r) , mp_n ) + mp_b*r + mp_offset , T(1.0) ),
                                         outPoint.m_ColorX, mp_origin);
            } else
            {
                outPoint.m_ColorX = Lerp(std::fmod( mp_b * r + mp_offset , T(1.0) ),
                                          outPoint.m_ColorX, mp_origin);
            }
        } 
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
        }
        else {
            helper.Out.x = m_Weight * helper.In.x;
            helper.Out.y = m_Weight * helper.In.y;
            helper.Out.z = m_Weight * helper.In.z;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_in = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_out = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_an = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_n = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_b = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_offset = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t r = sqrt( Sqr(vIn.x-"<< mp_x <<") + Sqr(vIn.y-"<< mp_y <<") ); \n"
		      "\t\t if( r >= "<< mp_in <<" && (r <= "<< mp_out <<" || "<< mp_out <<" <0 ) ) { \n"
		      "\t\t\t if("<< mp_an <<" !=0 && "<< mp_n <<" != 0) { \n"
		      "\t\t\t\t outPoint->m_ColorX = Lerp(fmod( "<< mp_an <<" * pow( Zeps(r) , "<< mp_n <<" ) + "<< mp_b <<"*r + "<< mp_offset <<" , (real_t)(1.0) ), \n"
		      "\t\t\t\t\t\t\t\t\t\t outPoint->m_ColorX, "<< mp_origin <<"); \n"
		      "\t\t\t } else \n"
		      "\t\t\t { \n"
		      "\t\t\t\t outPoint->m_ColorX = Lerp(fmod( "<< mp_b <<" * r + "<< mp_offset <<" , (real_t)(1.0) ), \n"
		      "\t\t\t\t\t\t\t\t\t\t  outPoint->m_ColorX, "<< mp_origin <<"); \n"
		      "\t\t\t } \n"
		      "\t\t } \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

	//virtual void Precalc() override;

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Sqr","Zeps","Lerp" };
	}

protected:
	void Init() {
        string prefix = Prefix()+"dc_ring_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_in, prefix + "in",0));
        m_Params.push_back(ParamWithName<T>(&mp_out, prefix + "out",1));
        m_Params.push_back(ParamWithName<T>(&mp_an, prefix + "an",0));
        m_Params.push_back(ParamWithName<T>(&mp_n, prefix + "n",0));
        m_Params.push_back(ParamWithName<T>(&mp_b, prefix + "b",1));
        m_Params.push_back(ParamWithName<T>(&mp_offset, prefix + "offset",0));
        m_Params.push_back(ParamWithName<T>(&mp_x, prefix + "x",0));
        m_Params.push_back(ParamWithName<T>(&mp_y, prefix + "y",0));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
    }

private:
	T mp_in;
	T mp_out;
	T mp_an;
	T mp_n;
	T mp_b;
	T mp_offset;
	T mp_x;
	T mp_y;
	T mp_origin;
};
MAKEPREPOSTPLUGINPARVAR(DCRingVariation, dc_ring) ;

/// <summary>
/// DC Step.
/// color= Round( (color+offset0)*n ) / n - offset0 + + mp_offset1
/// blend with original color with "origin"
/// </summary>
/// varFullName : DCStep
/// stringName  : dc_step
template <typename T>
class DCStep : public ParametricVariation<T>
{
public:
	DCStep(T weight = 1.0) : ParametricVariation<T>("dc_step", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCStep)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T newColor=fmod( ( Round( outPoint.m_ColorX*mp_n+mp_offset0 )- mp_offset0 )*pc_1n  + mp_offset1, T(1));
        outPoint.m_ColorX = Lerp( newColor<0 ? newColor+1 : newColor ,
                            outPoint.m_ColorX, mp_origin);
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
        }
        else {
            helper.Out.x = m_Weight * helper.In.x;
            helper.Out.y = m_Weight * helper.In.y;
            helper.Out.z = m_Weight * helper.In.z;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_n = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_offset0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_offset1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_1n = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t newColor=fmod( ( Round( outPoint->m_ColorX*"<< mp_n <<"+"<< mp_offset0 <<" )- "<< mp_offset0 <<" )*"<< pc_1n <<" + "<< mp_offset1 <<", (real_t)(1)); \n"
		      "\t\t outPoint->m_ColorX = Lerp( newColor<0 ? newColor+1 : newColor , \n"
		      "\t\t\t\t\t\t\t outPoint->m_ColorX, "<< mp_origin <<"); \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        if(IsNearZero(mp_n))
            pc_1n=0;
        else
            pc_1n=1/mp_n;
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round","Lerp" };
	}

protected:
	void Init() {
        string prefix = Prefix()+"dc_step_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_n, prefix + "n",2));
        m_Params.push_back(ParamWithName<T>(&mp_offset0, prefix + "offset0",0));
        m_Params.push_back(ParamWithName<T>(&mp_offset1, prefix + "offset1",0));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_1n, prefix + "1n"));
    }

private:
	T mp_n;
	T mp_offset0;
	T mp_offset1;
	T mp_origin;
    T pc_1n;
};
MAKEPREPOSTPLUGINPARVAR(DCStep, dc_step) ;

/// <summary>
/// DC Rand Check.
/// generate random DC color checks.
/// </summary>
/// varFullName : DCRandCheck
/// stringName  : dc_randcheck
template <typename T>
class DCRandCheck : public ParametricVariation<T>
{
public:
	DCRandCheck(T weight = 1.0) : ParametricVariation<T>("dc_randcheck", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCRandCheck)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        float nx=(int)Floor<T>((helper.In.x-mp_x0) * pc_ex);
        float ny=(int)Floor<T>((helper.In.y-mp_y0) * pc_ey);
        float seed=(float)mp_seed;
        float seed2=(float)mp_seed2;
        T newColor=fmod( (float)pc_rand[( (int)(25.263f*nx*(seed+7.9112f)+ny*3.2651f*(seed2+11.335f)+7.1231f) % COUNT +COUNT) %COUNT] * 40.175f
                + (float)pc_rand[( (int)(nx*53.419f*(seed2+3.7211f)+ny*488.47f) % COUNT +COUNT) %COUNT ] *(-233.93f)
                + (float)pc_rand[( (int)((float)pc_rand[( (int)((nx-17.252f*ny)*(seed+43.865f)) % COUNT +COUNT) %COUNT ]*(seed+1.5585f)) % COUNT +COUNT) %COUNT] * 0.51889f
                + (float)pc_rand[( (int)((float)pc_rand[( (int)(ny*(seed2+3.7773f)+15.325f*nx*(seed+6.8421f)+2.2863f) % COUNT +COUNT) %COUNT ]*(seed+9.21f)) % COUNT +COUNT) %COUNT] * 708.96f
                + (float)mp_offset, 1.0f);
        newColor=Lerp(mp_A,mp_B,newColor<0 ? newColor+1 : newColor);
        outPoint.m_ColorX = Lerp( newColor<0 ? newColor+1 : newColor ,
                            outPoint.m_ColorX, mp_origin);
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
        }
        else {
            helper.Out.x = m_Weight * helper.In.x;
            helper.Out.y = m_Weight * helper.In.y;
            helper.Out.z = m_Weight * helper.In.z;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_x0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_seed = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_seed2 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_A = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_B = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_offset = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ex = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ey = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rand = ToUpper(m_Params[i].Name()) + stateIndex; i+=COUNT;
		
		ss << "\t {\n";
		ss << "\t\t const int COUNT=17; \n"
		      "\t\t const int _pc_rand_="<< pc_rand <<"; \n"
		      "\t\t float nx=(int)floor((vIn.x-"<< mp_x0 <<") * "<< pc_ex <<"); \n"
		      "\t\t float ny=(int)floor((vIn.y-"<< mp_y0 <<") * "<< pc_ey <<"); \n"
		      "\t\t float seed=(float)"<< mp_seed <<"; \n"
		      "\t\t float seed2=(float)"<< mp_seed2 <<"; \n"
		      "\t\t real_t newColor=fmod( (float)parVars[_pc_rand_+( (int)(25.263f*nx*(seed+7.9112f)+ny*3.2651f*(seed2+11.335f)+7.1231f) % COUNT +COUNT) %COUNT] * 40.175f \n"
		      "\t\t\t\t + (float)parVars[_pc_rand_+( (int)(nx*53.419f*(seed2+3.7211f)+ny*488.47f) % COUNT +COUNT) %COUNT ] *(-233.93f) \n"
		      "\t\t\t\t + (float)parVars[_pc_rand_+( (int)((float)parVars[_pc_rand_+( (int)((nx-17.252f*ny)*(seed+43.865f)) % COUNT +COUNT) %COUNT ]*(seed+1.5585f)) % COUNT +COUNT) %COUNT] * 0.51889f \n"
		      "\t\t\t\t + (float)parVars[_pc_rand_+( (int)((float)parVars[_pc_rand_+( (int)(ny*(seed2+3.7773f)+15.325f*nx*(seed+6.8421f)+2.2863f) % COUNT +COUNT) %COUNT ]*(seed+9.21f)) % COUNT +COUNT) %COUNT] * 708.96f \n"
		      "\t\t\t\t + (float)"<< mp_offset <<", 1.0f); \n"
		      "\t\t newColor=Lerp("<< mp_A <<","<< mp_B <<",newColor<0 ? newColor+1 : newColor); \n"
		      "\t\t outPoint->m_ColorX = Lerp( newColor<0 ? newColor+1 : newColor , \n"
		      "\t\t\t\t\t\t\t outPoint->m_ColorX, "<< mp_origin <<"); \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
	}

	virtual void Precalc() override
    {
        pc_ex=IsNearZero(mp_x1-mp_x0) ? 0 : 1/(mp_x1-mp_x0);
        pc_ey=IsNearZero(mp_y1-mp_y0) ? 0 : 1/(mp_y1-mp_y0);
        float seed=(float)mp_seed;
        float seed2=(float)mp_seed2;
        float prev=VarFuncs<float>::Fract(seed*seed2+0.772);
        for(int i=0;i<COUNT;++i) {
            if(i%3==0)
                prev=VarFuncs<float>::HashShadertoy(prev*17.24563+i*68.4081, 6*i*prev*(seed2+7.33), prev*0.3+seed);
            else
                prev=VarFuncs<float>::HashShadertoy(prev*i*43+seed, 23.325*i*i+prev, (seed2+5.2)*prev);
            pc_rand[i]=prev;
        }
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Lerp" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"dc_randcheck_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_x0, prefix + "x0",0));
        m_Params.push_back(ParamWithName<T>(&mp_x1, prefix + "x1",1));
        m_Params.push_back(ParamWithName<T>(&mp_y0, prefix + "y0",0));
        m_Params.push_back(ParamWithName<T>(&mp_y1, prefix + "y1",1));
        m_Params.push_back(ParamWithName<T>(&mp_seed, prefix + "seed",0));
        m_Params.push_back(ParamWithName<T>(&mp_seed2, prefix + "seed2",0));
        m_Params.push_back(ParamWithName<T>(&mp_A, prefix + "A",0));
        m_Params.push_back(ParamWithName<T>(&mp_B, prefix + "B",1));
        m_Params.push_back(ParamWithName<T>(&mp_offset, prefix + "offset",0));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_ex, prefix + "ex"));
		m_Params.push_back(ParamWithName<T>(true,&pc_ey, prefix + "ey"));
        for(int i=0;i<COUNT;++i) 
            m_Params.push_back(ParamWithName<T>(true,&pc_rand[i], prefix + "rand"+to_string(i)));
    }

private:
    static const int COUNT=17;
	T mp_x0;
	T mp_x1;
	T mp_y0;
	T mp_y1;
    T mp_seed;
    T mp_seed2;
    T mp_A;
    T mp_B;
	T mp_offset;
    T mp_origin;
	T pc_ex;
    T pc_ey;
    T pc_rand[COUNT];

};
MAKEPREPOSTPLUGINPARVAR(DCRandCheck, dc_randcheck) ;

/// <summary>
/// DC Check.
/// Generate two DC color checks.
/// </summary>
/// varFullName : DCCheck
/// stringName  : dc_check
template <typename T>
class DCCheck : public ParametricVariation<T>
{
public:
	DCCheck(T weight = 1.0) : ParametricVariation<T>("dc_check", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCCheck)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        int drop=0;
        if( ( (int)Floor<T>((helper.In.x-mp_x0) * pc_ex)+(int)Floor<T>((helper.In.y-mp_y0) * pc_ey) )%2==0 ) {
            //color A
            if(rand.Frand01<T>() < mp_oA) 
                outPoint.m_ColorX = Lerp( mp_cA, outPoint.m_ColorX, mp_origin);
            else if(mp_hollow) drop=1;
        } else {
            //color B
            if(rand.Frand01<T>() < mp_oB) 
                outPoint.m_ColorX = Lerp( mp_cB, outPoint.m_ColorX, mp_origin);
            else if(mp_hollow) drop=1;
            
        }
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
            if(drop) {
                helper.Out.x = helper.Out.y = mp_dropoff;
                helper.Out.z=0;
                outPoint.m_X = outPoint.m_Y = mp_dropoff;
                outPoint.m_Z=0;
            }
        }
        else {
            if(drop) {
                helper.Out.x = helper.Out.y = mp_dropoff;
                helper.Out.z=0;
            }
            else {
                helper.Out.x = m_Weight * helper.In.x;
                helper.Out.y = m_Weight * helper.In.y;
                helper.Out.z = m_Weight * helper.In.z;
            }
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_x0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cA = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cB = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_oA = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_oB = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_hollow = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_dropoff = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ex = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ey = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t int drop=0; \n"
		      "\t\t if( ( (int)floor((vIn.x-"<< mp_x0 <<") * "<< pc_ex <<")+(int)floor((vIn.y-"<< mp_y0 <<") * "<< pc_ey <<") )%2==0 ) { \n"
		      "\t\t\t if(MwcNext01(mwc) < "<< mp_oA <<") \n"
		      "\t\t\t\t outPoint->m_ColorX = Lerp( "<< mp_cA <<", outPoint->m_ColorX, "<< mp_origin <<"); \n"
		      "\t\t\t else if("<< mp_hollow <<") drop=1; \n"
		      "\t\t } else { \n"
		      "\t\t\t if(MwcNext01(mwc) < "<< mp_oB <<") \n"
		      "\t\t\t\t outPoint->m_ColorX = Lerp( "<< mp_cB <<", outPoint->m_ColorX, "<< mp_origin <<"); \n"
		      "\t\t\t else if("<< mp_hollow <<") drop=1; \n"
		      "\t\t } \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n"
		      "\t\t\t if(drop) { \n"
		      "\t\t\t\t vOut.x = vOut.y = "<< mp_dropoff <<"; \n"
		      "\t\t\t\t vOut.z=0; \n"
		      "\t\t\t\t outPoint->m_X = outPoint->m_Y = "<< mp_dropoff <<"; \n"
		      "\t\t\t\t outPoint->m_Z=0; \n"
		      "\t\t\t } \n";
		} 
		else { 
		ss << "\t\t\t if(drop) { \n"
		      "\t\t\t\t vOut.x = vOut.y = "<< mp_dropoff <<"; \n"
		      "\t\t\t\t vOut.z=0; \n"
		      "\t\t\t } \n"
		      "\t\t\t else { \n"
		      "\t\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t\t vOut.z = "<< weight <<" * vIn.z; \n"
		      "\t\t\t } \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_ex=IsNearZero(mp_x1-mp_x0) ? 0 : 1/(mp_x1-mp_x0);
        pc_ey=IsNearZero(mp_y1-mp_y0) ? 0 : 1/(mp_y1-mp_y0);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Lerp" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"dc_check_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_x0, prefix + "x0",0));
        m_Params.push_back(ParamWithName<T>(&mp_x1, prefix + "x1",1));
        m_Params.push_back(ParamWithName<T>(&mp_y0, prefix + "y0",0));
        m_Params.push_back(ParamWithName<T>(&mp_y1, prefix + "y1",1));
        m_Params.push_back(ParamWithName<T>(&mp_cA, prefix + "cA",0));
        m_Params.push_back(ParamWithName<T>(&mp_cB, prefix + "cB",0.5));
        m_Params.push_back(ParamWithName<T>(&mp_oA, prefix + "oA",1,eParamType::REAL,0,1));
        m_Params.push_back(ParamWithName<T>(&mp_oB, prefix + "oB",1,eParamType::REAL,0,1));
        m_Params.push_back(ParamWithName<T>(&mp_hollow, prefix + "hollow",0,eParamType::INTEGER,0,1));
        m_Params.push_back(ParamWithName<T>(&mp_dropoff, prefix + "dropoff",1e5));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_ex, prefix + "ex"));
		m_Params.push_back(ParamWithName<T>(true,&pc_ey, prefix + "ey"));
    }

private:
	T mp_x0;
	T mp_x1;
	T mp_y0;
	T mp_y1;
    T mp_cA;
    T mp_cB;
    T mp_oA;
    T mp_oB;
    T mp_hollow;
    T mp_dropoff;
    T mp_origin;
	T pc_ex;
    T pc_ey;


};
MAKEPREPOSTPLUGINPARVAR(DCCheck, dc_check) ;


/// <summary>
/// DC Pie.
/// color=fmod( K*ang + rotate   ,1) 
/// ang in normalized -0.5~0.5
/// color is stepped, than maped between A~B
/// blend with original color with "origin"
/// </summary>
/// varFullName : DCPie
/// stringName  : dc_pie
template <typename T>
class DCPie : public ParametricVariation<T>
{
public:
	DCPie(T weight = 1.0) : ParametricVariation<T>("dc_pie", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(DCPie)


	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {   
        T ang = atan2(helper.In.y-mp_y,helper.In.x-mp_x) * T(M_1_PI) * T(0.5);
        T c= fmod( mp_K*ang + mp_rotate, T(1.0) );
        if(c<0) c+=T(1.0);
        if(mp_step!=0) {
            c=fmod( ( Round( c*mp_step+mp_offset)- mp_offset)*pc_1step, T(1) );
            if(c<0) c+=T(1.0);
        }
        c=fmod( Lerp(mp_cStart,mp_cEnd,c)+ mp_cOffset, T(1.0) );
        if(c<0) c+=T(1.0);
        outPoint.m_ColorX = Lerp( c, outPoint.m_ColorX, mp_origin );
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = helper.Out.z = 0;
        }
        else {
            helper.Out.x = m_Weight * helper.In.x;
            helper.Out.y = m_Weight * helper.In.y;
            helper.Out.z = m_Weight * helper.In.z;
        }
    }

	virtual string OpenCLString() const override
    {	        
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_K = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rotate = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cStart = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cEnd = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cOffset = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_step = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_offset = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_1step = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t ang = atan2(vIn.y-"<< mp_y <<",vIn.x-"<< mp_x <<") * (real_t)(M_1_PI) * (real_t)(0.5); \n"
		      "\t\t real_t c= fmod( "<< mp_K <<"*ang + "<< mp_rotate <<", (real_t)(1.0) ); \n"
		      "\t\t if(c<0) c+=(real_t)(1.0); \n"
		      "\t\t if("<< mp_step <<"!=0) { \n"
		      "\t\t\t c=fmod( ( Round( c*"<< mp_step <<"+"<< mp_offset <<")- "<< mp_offset <<")*"<< pc_1step <<", (real_t)(1) ); \n"
		      "\t\t\t if(c<0) c+=(real_t)(1.0); \n"
		      "\t\t } \n"
		      "\t\t c=fmod( Lerp("<< mp_cStart <<","<< mp_cEnd <<",c)+ "<< mp_cOffset <<", (real_t)(1.0) ); \n"
		      "\t\t if(c<0) c+=(real_t)(1.0); \n"
		      "\t\t outPoint->m_ColorX = Lerp( c, outPoint->m_ColorX, "<< mp_origin <<" ); \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

    virtual void Precalc() override
    {
        pc_1step=IsNearZero(mp_step) ? 0 : 1/(mp_step);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round","Lerp" };
	}

protected:
	void Init() {
        string prefix = Prefix()+"dc_pie_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_K, prefix + "K",1));
        m_Params.push_back(ParamWithName<T>(&mp_rotate, prefix + "rotate",0));
        m_Params.push_back(ParamWithName<T>(&mp_x, prefix + "x",0));
        m_Params.push_back(ParamWithName<T>(&mp_y, prefix + "y",0));
        m_Params.push_back(ParamWithName<T>(&mp_cStart, prefix + "cStart",0));
        m_Params.push_back(ParamWithName<T>(&mp_cEnd, prefix + "cEnd",1));
        m_Params.push_back(ParamWithName<T>(&mp_cOffset, prefix + "cOffset",0));
        m_Params.push_back(ParamWithName<T>(&mp_step, prefix + "step",5));
        m_Params.push_back(ParamWithName<T>(&mp_offset, prefix + "offset",0));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_1step, prefix + "1step"));
        
    }

private:
	T mp_K;
    T mp_rotate;
    T mp_x;
    T mp_y;
    T mp_cStart;
    T mp_cEnd;
    T mp_cOffset;
    T mp_step;
    T mp_offset;
    T mp_origin;
    T pc_1step;
};
MAKEPREPOSTPLUGINPARVAR(DCPie, dc_pie) ;

/// <summary>
/// Map color index (c, normalize 0~1 in cA~cB) to Z value.
/// z = exp*e^(E*c) -1 + An*c^n + B*c + C + origin * origin_Z
/// </summary>
/// varFullName : Color2Z
/// stringName  : color2z
template <typename T>
class Color2Z : public ParametricVariation<T>
{
public:
	Color2Z(T weight = 1.0) : ParametricVariation<T>("color2z", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(Color2Z)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T c= Clamp( (outPoint.m_ColorX-mp_cA) * pc_ec , T(0), T(1));
        T z= mp_B*c + mp_C + mp_origin * helper.In.z;
        if( mp_E !=0) 
            z+=mp_exp*(std::exp(mp_E*c) - 1);
        if( mp_An !=0 && mp_N !=0) 
            z+=mp_An * pow( Zeps(c) , mp_N );
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = 0;
        }
        else {
            helper.Out.x = helper.In.x;
            helper.Out.y = helper.In.y;
        }
		helper.Out.z = m_Weight * z;
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_cA = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cB = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_An = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_N = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_B = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_exp = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_E = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_C = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_origin = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ec = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t c= clamp( (outPoint->m_ColorX-"<< mp_cA <<") * "<< pc_ec <<" , (real_t)(0), (real_t)(1)); \n"
		      "\t\t real_t z= "<< mp_B <<"*c + "<< mp_C <<" + "<< mp_origin <<" * vIn.z; \n"
		      "\t\t if( "<< mp_E <<" !=0) \n"
		      "\t\t\t z+="<< mp_exp <<"*(exp("<< mp_E <<"*c) - 1); \n"
		      "\t\t if( "<< mp_An <<" !=0 && "<< mp_N <<" !=0) \n"
		      "\t\t\t z+="<< mp_An <<" * pow( Zeps(c) , "<< mp_N <<" ); \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = vIn.x; \n"
		      "\t\t\t vOut.y = vIn.y; \n";
		} 
		ss << "\t\t vOut.z = "<< weight <<" * z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_ec=IsNearZero(mp_cB-mp_cA) ? 0 : 1/(mp_cB-mp_cA);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Zeps" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"color2z_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_cA, prefix + "cA",0));
        m_Params.push_back(ParamWithName<T>(&mp_cB, prefix + "cB",1));
        m_Params.push_back(ParamWithName<T>(&mp_An, prefix + "An",0));
        m_Params.push_back(ParamWithName<T>(&mp_N, prefix + "N",0));
        m_Params.push_back(ParamWithName<T>(&mp_B, prefix + "B",1));
        m_Params.push_back(ParamWithName<T>(&mp_exp, prefix + "exp",1));
        m_Params.push_back(ParamWithName<T>(&mp_E, prefix + "E",0));
        m_Params.push_back(ParamWithName<T>(&mp_C, prefix + "C",0));
        m_Params.push_back(ParamWithName<T>(&mp_origin, prefix + "origin",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_ec, prefix + "ec"));
    }

private:
	T mp_cA;
	T mp_cB;
    T mp_An;
	T mp_N;
    T mp_B;
    T mp_exp;
    T mp_E;
	T mp_C;
    T mp_origin;
	T pc_ec;



};
MAKEPREPOSTPLUGINPARVAR(Color2Z, color2z) ;



#define VAR_COUNT 60

Variation<float> * variationsFloat[VAR_COUNT];
Variation<double> * variationsDouble[VAR_COUNT];
VariationPluginInfo variationsInfo={0, variationsFloat,variationsDouble};

void initializeVariationList() {

    if(variationsInfo.size == 0) {
        int i=0;

        POPULATEVARLIST_REGPREPOST(DCOffset) ;
        POPULATEVARLIST_REGPREPOST(DCRingVariation) ;
        POPULATEVARLIST_REGPREPOST(DCStep) ;
        POPULATEVARLIST_REGPREPOST(DCRandCheck) ;
        POPULATEVARLIST_REGPREPOST(DCCheck) ;
        POPULATEVARLIST_REGPREPOST(DCPie) ;
        POPULATEVARLIST_REGPREPOST(Color2Z) ;

        
        
        variationsInfo.size=i;
    }
}

}

/// <summary>
/// Return Info of Variations contained in this plugin:
///   size and variation lists
/// </summary>
extern "C" 
VARPLUGINEXP VariationPluginInfo*  GetVariationList() {
    EmberNs::initializeVariationList();
    return & EmberNs::variationsInfo ;
}




