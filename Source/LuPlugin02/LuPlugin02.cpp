﻿#include "Variation.h"
#include "XForm.h"
#include "VariationPluginInfo.h"
#include "../EmberPluginMacro.h"
#include "../DllExport.h"


namespace EmberNs {

/// <summary>
/// BlurRing.
/// </summary>
/// varFullName : BlurRing
/// stringName  : blur_ring
template <typename T>
class BlurRing : public ParametricVariation<T>
{
public:
	BlurRing(T weight = 1.0) : ParametricVariation<T>("blur_ring", DUMMYVARID, weight)
	{
		Init();
	}

    PARVARCOPY(BlurRing)


	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
		T rad = std::sqrt( mp_start + rand.Frand01<T>() * (mp_end-mp_start) );
		T temp = rand.Frand01<T>() * T(M_2PI);
        if(m_VarType == eVariationType::VARTYPE_POST) {
            helper.Out.x = m_Weight * std::cos(temp) * rad + helper.In.x;
            helper.Out.y = m_Weight * std::sin(temp) * rad + helper.In.y;
            helper.Out.z = helper.In.z;
        }
        else  {
            helper.Out.x = m_Weight * std::cos(temp) * rad;
            helper.Out.y = m_Weight * std::sin(temp) * rad;
            helper.Out.z = 0;
        } 		
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_in = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_out = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_start = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_end = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t rad = sqrt( "<< mp_start <<" + MwcNext01(mwc) * ("<< mp_end <<"-"<< mp_start <<") ); \n"
		      "\t\t real_t temp = MwcNext01(mwc) * (real_t)(M_2PI); \n";
		if(m_VarType == eVariationType::VARTYPE_POST) { 
		ss << "\t\t\t vOut.x = "<< weight <<" * cos(temp) * rad + vIn.x; \n"
		      "\t\t\t vOut.y = "<< weight <<" * sin(temp) * rad + vIn.y; \n"
		      "\t\t\t vOut.z = vIn.z; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = "<< weight <<" * cos(temp) * rad; \n"
		      "\t\t\t vOut.y = "<< weight <<" * sin(temp) * rad; \n"
		      "\t\t\t vOut.z = 0; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
	{
		mp_start = mp_in * mp_in;
		mp_end = mp_out * mp_out;
	}

protected:
	void Init()
	{
		string prefix = Prefix()+"blur_ring_";
		m_Params.clear();
		m_Params.push_back(ParamWithName<T>(&mp_in, prefix + "in",0.5));
		m_Params.push_back(ParamWithName<T>(&mp_out, prefix + "out",1));
		m_Params.push_back(ParamWithName<T>(true,&mp_start, prefix + "start"));
		m_Params.push_back(ParamWithName<T>(true,&mp_end, prefix + "end"));
	}

private:
	T mp_in;
	T mp_out;
	T mp_start;
	T mp_end;
};
MAKEPREPOSTPLUGINPARVAR(BlurRing, blur_ring) ;

/// <summary>
/// Fast Grid.
/// Generate grids.
/// Mode: 0 shrink | 1 crop      
///     ---- maybe do later
///     : -1 shrink + drop y | -2 shrink + drop x
///     : -3 shrink + drop diagonal
///     : -4 shrink + mix
/// </summary>
/// varFullName : FastGrid
/// stringName  : fastgrid
template <typename T>
class FastGrid : public ParametricVariation<T>
{
public:
	FastGrid(T weight = 1.0) : ParametricVariation<T>("fastgrid", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(FastGrid)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T fx = VarFuncs<T>::Fract( (helper.In.x-mp_x0) * pc_ex );
        T fy = VarFuncs<T>::Fract( (helper.In.y-mp_y0) * pc_ey );
        if(mp_prob < T(1) && rand.Frand01<T>() > mp_prob) {
            helper.Out.x=m_Weight * helper.In.x;
            helper.Out.y=m_Weight * helper.In.y;
            helper.Out.z = DefaultZ(helper);

        } else {
            if(mp_mode==0) {
                if(rand.Frand01<T>() < 0.5) 
                    helper.Out.x=m_Weight*( ( fx<0.5 ? fx*mp_wx : pc_dx-(1-fx)*mp_wx ) + helper.In.x - fx*pc_dx );
                else 
                    helper.Out.y=m_Weight*( ( fy<0.5 ? fy*mp_wy : pc_dy-(1-fy)*mp_wy ) + helper.In.y - fy*pc_dy );
            }
            else {
                T dx=fx*pc_dx;
                T dy=fy*pc_dy;
                if( dx>0.5*mp_wx && dx<pc_dx-0.5*mp_wx && dy>0.5*mp_wy && dy<pc_dy-0.5*mp_wy)
                    helper.Out.x=helper.Out.y=DROPOFF;
            }
            helper.Out.z = DefaultZ(helper);
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_x0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_wx = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_wy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_prob = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_mode = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_dx = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_dy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ex = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_ey = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t const real_t DROPOFF=1e8; \n"
		      "\t\t real_t fx = Fract( (vIn.x-"<< mp_x0 <<") * "<< pc_ex <<" ); \n"
		      "\t\t real_t fy = Fract( (vIn.y-"<< mp_y0 <<") * "<< pc_ey <<" ); \n"
		      "\t\t if("<< mp_prob <<" < (real_t)(1) && MwcNext01(mwc) > "<< mp_prob <<") { \n"
		      "\t\t\t vOut.x="<< weight <<" * vIn.x; \n"
		      "\t\t\t vOut.y="<< weight <<" * vIn.y; \n"
		      "\t\t\t vOut.z = " << DefaultZCl() <<
		      "\t\t } else { \n"
		      "\t\t\t if("<< mp_mode <<"==0) { \n"
		      "\t\t\t\t if(MwcNext01(mwc) < 0.5) \n"
		      "\t\t\t\t\t vOut.x="<< weight <<"*( ( fx<0.5 ? fx*"<< mp_wx <<" : "<< pc_dx <<"-(1-fx)*"<< mp_wx <<" ) + vIn.x - fx*"<< pc_dx <<" ); \n"
		      "\t\t\t\t else \n"
		      "\t\t\t\t\t vOut.y="<< weight <<"*( ( fy<0.5 ? fy*"<< mp_wy <<" : "<< pc_dy <<"-(1-fy)*"<< mp_wy <<" ) + vIn.y - fy*"<< pc_dy <<" ); \n"
		      "\t\t\t } \n"
		      "\t\t\t else { \n"
		      "\t\t\t\t real_t dx=fx*"<< pc_dx <<"; \n"
		      "\t\t\t\t real_t dy=fy*"<< pc_dy <<"; \n"
		      "\t\t\t\t if( dx>0.5*"<< mp_wx <<" && dx<"<< pc_dx <<"-0.5*"<< mp_wx <<" && dy>0.5*"<< mp_wy <<" && dy<"<< pc_dy <<"-0.5*"<< mp_wy <<") \n"
		      "\t\t\t\t\t vOut.x=vOut.y=DROPOFF; \n"
		      "\t\t\t } \n"
		      "\t\t\t vOut.z = " << DefaultZCl() <<
		      "\t\t } \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_dx=mp_x1-mp_x0;
        pc_dy=mp_y1-mp_y0;
        pc_ex=IsNearZero(pc_dx) ? 0 : 1/(pc_dx);
        pc_ey=IsNearZero(pc_dy) ? 0 : 1/(pc_dy);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Fract" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"fastgrid_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_x0, prefix + "x0",0));
        m_Params.push_back(ParamWithName<T>(&mp_x1, prefix + "x1",1));
        m_Params.push_back(ParamWithName<T>(&mp_y0, prefix + "y0",0));
        m_Params.push_back(ParamWithName<T>(&mp_y1, prefix + "y1",1));
        m_Params.push_back(ParamWithName<T>(&mp_wx, prefix + "wx",0));
        m_Params.push_back(ParamWithName<T>(&mp_wy, prefix + "wy",0));
        m_Params.push_back(ParamWithName<T>(&mp_prob, prefix + "prob",1,eParamType::REAL,0,1));
        m_Params.push_back(ParamWithName<T>(&mp_mode, prefix + "mode",0,eParamType::INTEGER,0,1));
        m_Params.push_back(ParamWithName<T>(true,&pc_dx, prefix + "dx"));
        m_Params.push_back(ParamWithName<T>(true,&pc_dy, prefix + "dy"));
        m_Params.push_back(ParamWithName<T>(true,&pc_ex, prefix + "ex"));
		m_Params.push_back(ParamWithName<T>(true,&pc_ey, prefix + "ey"));
    }

private:
    const T DROPOFF=1e8;
    const string DROPOFF_S="1e8";
	T mp_x0;
	T mp_x1;
	T mp_y0;
	T mp_y1;
    T mp_wx;
    T mp_wy;
    T mp_prob;
    T mp_mode;
    T pc_dx;
    T pc_dy;
	T pc_ex;
    T pc_ey;
};
MAKEPREPOSTPLUGINPARVAR(FastGrid, fastgrid) ;


/// <summary>
/// Kage.
/// make copys of itself
/// </summary>
/// varFullName : Kage
/// stringName  : kage
template <typename T>
class Kage : public ParametricVariation<T>
{
public:
	Kage(T weight = 1.0) : ParametricVariation<T>("kage", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(Kage)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T n1= Round( rand.Frand01<T>()* (mp_L+1+mp_R )- T(0.5) ) - mp_L;
        T n2= Round( rand.Frand01<T>()* (mp_U+1+mp_D )- T(0.5) ) - mp_D;
        helper.Out.z = m_Weight * helper.In.z;
        if(mp_hide!=0 && n1==0 && n2==0 ) 
            helper.Out.x = helper.Out.y= DROPOFF;
        else {
            helper.Out.x = m_Weight * (helper.In.x + n1*pc_d1x+ n2*pc_d2x);
            helper.Out.y = m_Weight * (helper.In.y + n1*pc_d1y+ n2*pc_d2y);
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_L = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_R = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_dist1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ang1 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_U = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_D = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_dist2 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ang2 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_hide = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_d1x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_d1y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_d2x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_d2y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t const real_t DROPOFF=1e8; \n"
		      "\t\t real_t n1= Round( MwcNext01(mwc)* ("<< mp_L <<"+1+"<< mp_R <<" )- (real_t)(0.5) ) - "<< mp_L <<"; \n"
		      "\t\t real_t n2= Round( MwcNext01(mwc)* ("<< mp_U <<"+1+"<< mp_D <<" )- (real_t)(0.5) ) - "<< mp_D <<"; \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n"
		      "\t\t if("<< mp_hide <<"!=0 && n1==0 && n2==0 ) \n"
		      "\t\t\t vOut.x = vOut.y= DROPOFF; \n"
		      "\t\t else { \n"
		      "\t\t\t vOut.x = "<< weight <<" * (vIn.x + n1*"<< pc_d1x <<"+ n2*"<< pc_d2x <<"); \n"
		      "\t\t\t vOut.y = "<< weight <<" * (vIn.y + n1*"<< pc_d1y <<"+ n2*"<< pc_d2y <<"); \n"
		      "\t\t } \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_d1x=mp_dist1*cos(mp_ang1*M_PI);
        pc_d1y=mp_dist1*sin(mp_ang1*M_PI);
        pc_d2x=mp_dist2*cos(mp_ang2*M_PI);
        pc_d2y=mp_dist2*sin(mp_ang2*M_PI);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"kage_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_L, prefix + "L",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_R, prefix + "R",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_dist1, prefix + "dist1",1));
        m_Params.push_back(ParamWithName<T>(&mp_ang1, prefix + "ang1",0));
        m_Params.push_back(ParamWithName<T>(&mp_U, prefix + "U",0,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_D, prefix + "D",0,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_dist2, prefix + "dist2",1));
        m_Params.push_back(ParamWithName<T>(&mp_ang2, prefix + "ang2",0.5));
        m_Params.push_back(ParamWithName<T>(&mp_hide, prefix + "hide",0,eParamType::INTEGER,0,1));
        m_Params.push_back(ParamWithName<T>(true,&pc_d1x, prefix + "d1x"));
        m_Params.push_back(ParamWithName<T>(true,&pc_d1y, prefix + "d1y"));
        m_Params.push_back(ParamWithName<T>(true,&pc_d2x, prefix + "d2x"));
        m_Params.push_back(ParamWithName<T>(true,&pc_d2y, prefix + "d2y"));
    }

private:
    const T DROPOFF=1e8;
    const string DROPOFF_S="1e8";
    T mp_L;
    T mp_R;
	T mp_dist1;
	T mp_ang1;
    T mp_U;
    T mp_D;
	T mp_dist2;
	T mp_ang2;
    T mp_hide;
    T pc_d1x;
    T pc_d1y;
    T pc_d2x;
    T pc_d2y;
};
MAKEPREPOSTPLUGINPARVAR(Kage, kage) ;


/// <summary>
/// KageAng.
/// make copys of itself, rotate around x,y
/// </summary>
/// varFullName : Kage
/// stringName  : kage
template <typename T>
class KageAng : public ParametricVariation<T>
{
public:
	KageAng(T weight = 1.0) : ParametricVariation<T>("kage_ang", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(KageAng)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T n= Round( rand.Frand01<T>()* (mp_ccw+1+mp_cw )- T(0.5) ) - mp_ccw;
        T dx=helper.In.x-mp_x;
        T dy=helper.In.y-mp_y;
        T ang = std::atan2(dy,dx) + n*pc_angn + pc_rot_rad;
        T rad = VarFuncs<T>::Hypot(dx,dy)*mp_rscale;

        helper.Out.x = m_Weight * ( mp_x + rad*std::cos(ang) );
        helper.Out.y = m_Weight * ( mp_y + rad*std::sin(ang) );
        helper.Out.z = m_Weight * helper.In.z;
        
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_N = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ccw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rotate = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rscale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rot_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_angn = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t n= Round( MwcNext01(mwc)* ("<< mp_ccw <<"+1+"<< mp_cw <<" )- (real_t)(0.5) ) - "<< mp_ccw <<"; \n"
		      "\t\t real_t dx=vIn.x-"<< mp_x <<"; \n"
		      "\t\t real_t dy=vIn.y-"<< mp_y <<"; \n"
		      "\t\t real_t ang = atan2(dy,dx) + n*"<< pc_angn <<" + "<< pc_rot_rad <<"; \n"
		      "\t\t real_t rad = Hypot(dx,dy)*"<< mp_rscale <<"; \n"
		      "\t\t vOut.x = "<< weight <<" * ( "<< mp_x <<" + rad*cos(ang) ); \n"
		      "\t\t vOut.y = "<< weight <<" * ( "<< mp_y <<" + rad*sin(ang) ); \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_angn=T(2)*T(M_PI)/Zeps(mp_N);
        pc_rot_rad=mp_rotate*T(2)*T(M_PI);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round", "Hypot" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"kage_ang" "_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_N, prefix + "N",4));
        m_Params.push_back(ParamWithName<T>(&mp_ccw, prefix + "cw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_cw, prefix + "ccw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_rotate, prefix + "rotate",0));
        m_Params.push_back(ParamWithName<T>(&mp_rscale, prefix + "rscale",1));
        m_Params.push_back(ParamWithName<T>(&mp_x, prefix + "x",0));
        m_Params.push_back(ParamWithName<T>(&mp_y, prefix + "y",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_rot_rad, prefix + "rot_rad"));
        m_Params.push_back(ParamWithName<T>(true,&pc_angn, prefix + "angn"));

//        m_Params.push_back(ParamWithName<T>(true,&pc_d1x, prefix + "d1x"));

    }

private:

    T mp_N;
    T mp_ccw;
    T mp_cw;
	T mp_rotate;
    T mp_rscale;
    T mp_x;
    T mp_y;
    T pc_rot_rad;
    T pc_angn;

};
MAKEPREPOSTPLUGINPARVAR(KageAng, kage_ang) ;

/// <summary>
/// KageAng2.
/// Advanced version of KageAng
/// </summary>
/// varFullName : KageAng2
/// stringName  : kage_ang2
template <typename T>
class KageAng2 : public ParametricVariation<T>
{
public:
	KageAng2(T weight = 1.0) : ParametricVariation<T>("kage_ang2", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(KageAng2)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T n= Round( rand.Frand01<T>()* (mp_ccw+1+mp_cw )- T(0.5) ) - mp_ccw;
        
        T self_dx=helper.In.x-mp_self_x;
        T self_dy=helper.In.y-mp_self_y;
        T slef_ang = std::atan2(self_dy,self_dx);
        T rad_s = VarFuncs<T>::Hypot(self_dx,self_dy);
        int flip= ( (int)(n)%2==0? 1:-1) * (int)mp_flip;
        if(flip==1)
            slef_ang=pc_axis_rad-slef_ang;
        slef_ang += pc_rot_rad_s - mp_align*n*pc_angn ;
        T x = mp_self_x + rad_s*std::cos(slef_ang) ;
        T y = mp_self_y + rad_s*std::sin(slef_ang) ;
        
        T dx=x-mp_x;
        T dy=y-mp_y;
        T ang = std::atan2(dy,dx) + n*pc_angn + pc_rot_rad;
        T rad = VarFuncs<T>::Hypot(dx,dy)*mp_rscale;
        
        helper.Out.x = m_Weight * ( mp_x + rad*std::cos(ang) );
        helper.Out.y = m_Weight * ( mp_y + rad*std::sin(ang) );
        helper.Out.z = m_Weight * helper.In.z;
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_N = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ccw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rotate = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rscale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_align = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_rot = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_flip = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_axis = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rot_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rot_rad_s = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_angn = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_axis_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t n= Round( MwcNext01(mwc)* ("<< mp_ccw <<"+1+"<< mp_cw <<" )- (real_t)(0.5) ) - "<< mp_ccw <<"; \n"
		      "\t\t real_t self_dx=vIn.x-"<< mp_self_x <<"; \n"
		      "\t\t real_t self_dy=vIn.y-"<< mp_self_y <<"; \n"
		      "\t\t real_t slef_ang = atan2(self_dy,self_dx); \n"
		      "\t\t real_t rad_s = Hypot(self_dx,self_dy); \n"
		      "\t\t int flip= ( (int)(n)%2==0? 1:-1) * (int)"<< mp_flip <<"; \n"
		      "\t\t if(flip==1) \n"
		      "\t\t\t slef_ang="<< pc_axis_rad <<"-slef_ang; \n"
		      "\t\t slef_ang += "<< pc_rot_rad_s <<" - "<< mp_align <<"*n*"<< pc_angn <<" ; \n"
		      "\t\t real_t x = "<< mp_self_x <<" + rad_s*cos(slef_ang) ; \n"
		      "\t\t real_t y = "<< mp_self_y <<" + rad_s*sin(slef_ang) ; \n"
		      "\t\t real_t dx=x-"<< mp_x <<"; \n"
		      "\t\t real_t dy=y-"<< mp_y <<"; \n"
		      "\t\t real_t ang = atan2(dy,dx) + n*"<< pc_angn <<" + "<< pc_rot_rad <<"; \n"
		      "\t\t real_t rad = Hypot(dx,dy)*"<< mp_rscale <<"; \n"
		      "\t\t vOut.x = "<< weight <<" * ( "<< mp_x <<" + rad*cos(ang) ); \n"
		      "\t\t vOut.y = "<< weight <<" * ( "<< mp_y <<" + rad*sin(ang) ); \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_angn=T(2)*T(M_PI)/Zeps(mp_N);
        pc_rot_rad=mp_rotate*T(2)*T(M_PI);
        pc_rot_rad_s=mp_self_rot*T(2)*T(M_PI);
        pc_axis_rad=mp_axis*T(4)*T(M_PI);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round", "Hypot" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"kage_ang2" "_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_N, prefix + "N",4));
        m_Params.push_back(ParamWithName<T>(&mp_ccw, prefix + "cw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_cw, prefix + "ccw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_rotate, prefix + "rotate",0));
        m_Params.push_back(ParamWithName<T>(&mp_rscale, prefix + "rscale",1));
        m_Params.push_back(ParamWithName<T>(&mp_x, prefix + "x",0));
        m_Params.push_back(ParamWithName<T>(&mp_y, prefix + "y",0));
        m_Params.push_back(ParamWithName<T>(&mp_align, prefix + "align",0));
        m_Params.push_back(ParamWithName<T>(&mp_self_rot, prefix + "self_rot",0));
        m_Params.push_back(ParamWithName<T>(&mp_self_x, prefix + "self_x",0));
        m_Params.push_back(ParamWithName<T>(&mp_self_y, prefix + "self_y",0));
        m_Params.push_back(ParamWithName<T>(&mp_flip, prefix + "flip",0,eParamType::INTEGER,-1,1));
        m_Params.push_back(ParamWithName<T>(&mp_axis, prefix + "axis",0));

        m_Params.push_back(ParamWithName<T>(true,&pc_rot_rad, prefix + "rot_rad"));
        m_Params.push_back(ParamWithName<T>(true,&pc_rot_rad_s, prefix + "rot_rad_s"));
        m_Params.push_back(ParamWithName<T>(true,&pc_angn, prefix + "angn"));
        m_Params.push_back(ParamWithName<T>(true,&pc_axis_rad, prefix + "axis_rad"));

//        m_Params.push_back(ParamWithName<T>(true,&pc_d1x, prefix + "d1x"));

    }

private:

    T mp_N;
    T mp_ccw;
    T mp_cw;
	T mp_rotate;
    T mp_rscale;
    T mp_x;
    T mp_y;
    T mp_align;
    T mp_self_rot;
    T mp_self_x;
    T mp_self_y;
    T mp_flip;
    T mp_axis;
    T pc_rot_rad;
    T pc_rot_rad_s;
    T pc_angn;
    T pc_axis_rad;

};
MAKEPREPOSTPLUGINPARVAR(KageAng2, kage_ang2) ;

/// <summary>
/// KageAng2C.
/// Advanced version of KageAng
/// </summary>
/// varFullName : KageAng2C
/// stringName  : kage_ang2c
template <typename T>
class KageAng2C : public ParametricVariation<T>
{
public:
	KageAng2C(T weight = 1.0) : ParametricVariation<T>("kage_ang2c", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(KageAng2C)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T n= Round( rand.Frand01<T>()* (mp_ccw+1+mp_cw )- T(0.5) ) - mp_ccw;
        T ang = n*pc_angn + pc_phi_rad;
        T x,y;
        if(pc_shift==1) {
            x=helper.In.x;
            y=helper.In.y;
        } else {
            T self_dx=helper.In.x-mp_self_x;
            T self_dy=helper.In.y-mp_self_y;
            T slef_ang = std::atan2(self_dy,self_dx);
            T rad_s = VarFuncs<T>::Hypot(self_dx,self_dy)*mp_scale;
            int flip= ( (int)(n)%2==0? 1:-1) * (int)mp_flip;
            if(flip==1)
                slef_ang=pc_axis_rad-slef_ang;
            slef_ang += pc_rot_rad_s - (mp_align-1)*n*pc_angn ;
            x = mp_self_x + rad_s*std::cos(slef_ang) ;
            y = mp_self_y + rad_s*std::sin(slef_ang) ;
        }
        helper.Out.x = m_Weight * ( x + mp_r*std::cos(ang) );
        helper.Out.y = m_Weight * ( y + mp_r*std::sin(ang) );
        helper.Out.z = m_Weight * helper.In.z;
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_r = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_phi = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_N = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ccw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_cw = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_scale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_align = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_self_rot = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_flip = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_axis = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_phi_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rot_rad_s = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_angn = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_axis_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_shift = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t n= Round( MwcNext01(mwc)* ("<< mp_ccw <<"+1+"<< mp_cw <<" )- (real_t)(0.5) ) - "<< mp_ccw <<"; \n"
		      "\t\t real_t ang = n*"<< pc_angn <<" + "<< pc_phi_rad <<"; \n"
		      "\t\t real_t x,y; \n"
		      "\t\t if("<< pc_shift <<"==1) { \n"
		      "\t\t\t x=vIn.x; \n"
		      "\t\t\t y=vIn.y; \n"
		      "\t\t } else { \n"
		      "\t\t\t real_t self_dx=vIn.x-"<< mp_self_x <<"; \n"
		      "\t\t\t real_t self_dy=vIn.y-"<< mp_self_y <<"; \n"
		      "\t\t\t real_t slef_ang = atan2(self_dy,self_dx); \n"
		      "\t\t\t real_t rad_s = Hypot(self_dx,self_dy)*"<< mp_scale <<"; \n"
		      "\t\t\t int flip= ( (int)(n)%2==0? 1:-1) * (int)"<< mp_flip <<"; \n"
		      "\t\t\t if(flip==1) \n"
		      "\t\t\t\t slef_ang="<< pc_axis_rad <<"-slef_ang; \n"
		      "\t\t\t slef_ang += "<< pc_rot_rad_s <<" - ("<< mp_align <<"-1)*n*"<< pc_angn <<" ; \n"
		      "\t\t\t x = "<< mp_self_x <<" + rad_s*cos(slef_ang) ; \n"
		      "\t\t\t y = "<< mp_self_y <<" + rad_s*sin(slef_ang) ; \n"
		      "\t\t } \n"
		      "\t\t vOut.x = "<< weight <<" * ( x + "<< mp_r <<"*cos(ang) ); \n"
		      "\t\t vOut.y = "<< weight <<" * ( y + "<< mp_r <<"*sin(ang) ); \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_angn=T(2)*T(M_PI)/Zeps(mp_N);
        pc_phi_rad=mp_phi*T(2)*T(M_PI);
        pc_rot_rad_s=mp_self_rot*T(2)*T(M_PI);
        pc_axis_rad=mp_axis*T(4)*T(M_PI);
        pc_shift = (mp_scale ==T(1) && mp_align==T(1) && mp_self_rot==T(0) && mp_flip==T(0) ) ? T(1) : T(0);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round", "Hypot" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"kage_ang2c" "_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_r, prefix + "r",1));
        m_Params.push_back(ParamWithName<T>(&mp_phi, prefix + "phi",0));
        m_Params.push_back(ParamWithName<T>(&mp_N, prefix + "N",4));
        m_Params.push_back(ParamWithName<T>(&mp_ccw, prefix + "cw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_cw, prefix + "ccw",1,eParamType::INTEGER,0));
        m_Params.push_back(ParamWithName<T>(&mp_self_x, prefix + "self_x",0));
        m_Params.push_back(ParamWithName<T>(&mp_self_y, prefix + "self_y",0));
        m_Params.push_back(ParamWithName<T>(&mp_scale, prefix + "scale",1));
        m_Params.push_back(ParamWithName<T>(&mp_align, prefix + "align",1));
        m_Params.push_back(ParamWithName<T>(&mp_self_rot, prefix + "self_rot",0));
        m_Params.push_back(ParamWithName<T>(&mp_flip, prefix + "flip",0,eParamType::INTEGER,-1,1));
        m_Params.push_back(ParamWithName<T>(&mp_axis, prefix + "axis",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_phi_rad, prefix + "phi_rad"));
        m_Params.push_back(ParamWithName<T>(true,&pc_rot_rad_s, prefix + "rot_rad_s"));
        m_Params.push_back(ParamWithName<T>(true,&pc_angn, prefix + "angn"));
        m_Params.push_back(ParamWithName<T>(true,&pc_axis_rad, prefix + "axis_rad"));
        m_Params.push_back(ParamWithName<T>(true,&pc_shift, prefix + "shift"));
    }

private:

    T mp_r;
    T mp_phi;
    T mp_N;
    T mp_ccw;
    T mp_cw;
    T mp_self_x;
    T mp_self_y;
    T mp_scale;
    T mp_align;
    T mp_self_rot;
    T mp_flip;
    T mp_axis;
    T pc_phi_rad;
    T pc_rot_rad_s;
    T pc_angn;
    T pc_axis_rad;
    T pc_shift;

};
MAKEPREPOSTPLUGINPARVAR(KageAng2C, kage_ang2c) ;

/// <summary>
/// LinearLt.
/// Linear with w as weight, s as scale, d as move
/// used to quick modify variations.
/// </summary>
/// varFullName : LinearLt
/// stringName  : linearlt
template <typename T>
class LinearLt : public ParametricVariation<T>
{
public:
	LinearLt(T weight = 1.0) : ParametricVariation<T>("linearLT", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(LinearLt)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
            helper.Out.x = m_Weight * (helper.In.x * mp_sx * mp_w +mp_dx) ;
            helper.Out.y = m_Weight * (helper.In.y * mp_sy * mp_w +mp_dy) ;
            helper.Out.z = m_Weight * helper.In.z * mp_w;
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_w = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_sx = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_sy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_dx = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_dy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t vOut.x = "<< weight <<" * (vIn.x * "<< mp_sx <<" * "<< mp_w <<" + "<< mp_dx <<"); \n"
		      "\t\t vOut.y = "<< weight <<" * (vIn.y * "<< mp_sy <<" * "<< mp_w <<" + "<< mp_dy <<"); \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z * "<< mp_w <<"; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

protected:
    void Init() {
        string prefix = Prefix()+"linearLT""_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_w, prefix + "w",1));
        m_Params.push_back(ParamWithName<T>(&mp_sx, prefix + "sx",1));
        m_Params.push_back(ParamWithName<T>(&mp_sy, prefix + "sy",1));
        m_Params.push_back(ParamWithName<T>(&mp_dx, prefix + "dx",0));
        m_Params.push_back(ParamWithName<T>(&mp_dy, prefix + "dy",0));
    }

private:
    T mp_w;
    T mp_sx;
    T mp_sy;
    T mp_dx;
    T mp_dy;

};
MAKEPREPOSTPLUGINPARVAR(LinearLt, linearLT) ;

/// <summary>
/// BlurZ2.
/// blur to certain z, with gap and tail.
/// </summary>
/// varFullName : BlurZ2
/// stringName  : blurz2
template <typename T>
class BlurZ2 : public ParametricVariation<T>
{
public:
	BlurZ2(T weight = 1.0) : ParametricVariation<T>("blurz2", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(BlurZ2)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T z=0;
        if(mp_prob==T(1) || rand.Frand01<T>() < mp_prob) {
            T d = helper.In.z-mp_to;
            int pos_d= d>0 ? 1: 0 ;
            d=pos_d ? d : -d;
            T gapped=std::max( d-mp_gap,T(0));
            z = gapped * rand.Frand01<T>() + std::min(d,mp_gap);
            if(mp_tail !=0) {
                z += (mp_tail)*abs(rand.Frand01<T>()+rand.Frand01<T>()+rand.Frand01<T>()+rand.Frand01<T>()-T(2))  ;
            }
            if(pos_d==0)
                z=-z;
        }
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = 0;
            helper.Out.z = -z * m_Weight;
        }
        else {
            helper.Out.x = helper.In.x;
            helper.Out.y = helper.In.y;
            helper.Out.z = helper.In.z - z * m_Weight ;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_to = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_gap = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_tail = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_prob = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t z=0; \n"
		      "\t\t if("<< mp_prob <<"==(real_t)(1) || MwcNext01(mwc) < "<< mp_prob <<") { \n"
		      "\t\t\t real_t d = vIn.z-"<< mp_to <<"; \n"
		      "\t\t\t int pos_d= d>0 ? 1: 0 ; \n"
		      "\t\t\t d=pos_d ? d : -d; \n"
		      "\t\t\t real_t gapped=max( d-"<< mp_gap <<",(real_t)(0)); \n"
		      "\t\t\t z = gapped * MwcNext01(mwc) + min(d,"<< mp_gap <<"); \n"
		      "\t\t\t if("<< mp_tail <<" !=0) { \n"
		      "\t\t\t\t z += ("<< mp_tail <<")*fabs(MwcNext01(mwc)+MwcNext01(mwc)+MwcNext01(mwc)+MwcNext01(mwc)-(real_t)(2)) ; \n"
		      "\t\t\t } \n"
		      "\t\t\t if(pos_d==0) \n"
		      "\t\t\t\t z=-z; \n"
		      "\t\t } \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = 0; \n"
		      "\t\t\t vOut.z = -z * "<< weight <<"; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = vIn.x; \n"
		      "\t\t\t vOut.y = vIn.y; \n"
		      "\t\t\t vOut.z = vIn.z - z * "<< weight <<" ; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

protected:
    void Init() {
        string prefix = Prefix()+"blurz2""_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_to, prefix + "to",0));
        m_Params.push_back(ParamWithName<T>(&mp_gap, prefix + "gap",0));
        m_Params.push_back(ParamWithName<T>(&mp_tail, prefix + "tail",0));
        m_Params.push_back(ParamWithName<T>(&mp_prob, prefix + "prob",1,eParamType::REAL,0,1));
    }

private:
    T mp_to;
    T mp_gap;
    T mp_tail;
    T mp_prob;
};
MAKEPREPOSTPLUGINPARVAR(BlurZ2, blurz2) ;

/// <summary>
/// BlurZTail.
/// blur z with single sided tail and gap.
/// </summary>
/// varFullName : BlurZTail
/// stringName  : blurztail
template <typename T>
class BlurZTail : public ParametricVariation<T>
{
public:
	BlurZTail(T weight = 1.0) : ParametricVariation<T>("blurztail", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(BlurZTail)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T z=0;
        if(mp_prob==T(1) || rand.Frand01<T>() < mp_prob) {
            z=mp_gap;
            if(mp_B4!=0)
                z+=mp_B4*abs(rand.Frand01<T>()+rand.Frand01<T>()+rand.Frand01<T>()+rand.Frand01<T>()-T(2) ); 
            if(mp_An!=0 && mp_N!=0) 
                z+= mp_An* pow(Zeps(rand.Frand01<T>()+rand.Frand01<T>() )*0.5,mp_N);
        }
        if(m_VarType == eVariationType::VARTYPE_REG) {
            helper.Out.x = helper.Out.y = 0;
            helper.Out.z = -z * m_Weight;
        }
        else {
            helper.Out.x = helper.In.x;
            helper.Out.y = helper.In.y;
            helper.Out.z = helper.In.z - z* m_Weight ;
        }
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_B4 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_An = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_N = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_gap = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_prob = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t z=0; \n"
		      "\t\t if("<< mp_prob <<"==(real_t)(1) || MwcNext01(mwc) < "<< mp_prob <<") { \n"
		      "\t\t\t z="<< mp_gap <<"; \n"
		      "\t\t\t if("<< mp_B4 <<"!=0) \n"
		      "\t\t\t\t z+="<< mp_B4 <<"*fabs(MwcNext01(mwc)+MwcNext01(mwc)+MwcNext01(mwc)+MwcNext01(mwc)-(real_t)(2) ); \n"
		      "\t\t\t if("<< mp_An <<"!=0 && "<< mp_N <<"!=0) \n"
		      "\t\t\t\t z+= "<< mp_An <<"* pow(Zeps(MwcNext01(mwc)+MwcNext01(mwc) )*0.5,"<< mp_N <<"); \n"
		      "\t\t } \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = 0; \n"
		      "\t\t\t vOut.z = -z * "<< weight <<"; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = vIn.x; \n"
		      "\t\t\t vOut.y = vIn.y; \n"
		      "\t\t\t vOut.z = vIn.z - z* "<< weight <<" ; \n";
		} 
		
		ss << "\t }\n";
		return ss.str();
    }

    virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Zeps" };
	}
    
protected:
    void Init() {
        string prefix = Prefix()+"blurztail""_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_B4, prefix + "B4",0.3));
        m_Params.push_back(ParamWithName<T>(&mp_An, prefix + "An",0));
        m_Params.push_back(ParamWithName<T>(&mp_N, prefix + "N",0));
        m_Params.push_back(ParamWithName<T>(&mp_gap, prefix + "gap",0));
        m_Params.push_back(ParamWithName<T>(&mp_prob, prefix + "prob",1,eParamType::REAL,0,1));
    }

private:
    T mp_B4;
    T mp_An;
    T mp_N;
    T mp_gap;
    T mp_prob;
};
MAKEPREPOSTPLUGINPARVAR(BlurZTail, blurztail) ;

/// <summary>
/// ModZ.
/// modification to z.
/// z = Lerp(  Exp*e^(E*z) -1 +An*z^na + Bn*z^nb + C, z, origin)
/// </summary>
/// varFullName : ModZ
/// stringName  : modz
template <typename T>
class ZXYMod : public ParametricVariation<T>
{
public:
	ZXYMod(T weight = 1.0) : ParametricVariation<T>("zxymod", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(ZXYMod)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T in; 
		switch ((int)mp_zxy) {
		case 0:
			in=helper.In.z;
			break;
		case 1:
			in=helper.In.x;
			break;
		default:
			in=helper.In.y;
			break;
		}
		T v=mp_L0*in+mp_C;
		if( mp_E !=0) 
			v+=mp_exp*std::exp(mp_E*in) - 1;
		if( mp_An !=0) 
			v+=mp_An * (mp_Na==0? 1:  ( mp_Na==1 ? in : (in<=0? 0: std::pow(in , mp_Na)) ));
		if( mp_Bn !=0) 
			v+=mp_Bn * (mp_Nb==0? 1:  ( mp_Nb==1 ? in : (in<=0? 0: std::pow(in , mp_Nb)) ));
		if( mp_log !=0) 
			v+=mp_log * ( mp_G * in<=0? 0: std::log(mp_G*in+1));
		v = m_Weight * v;
		if(m_VarType == eVariationType::VARTYPE_REG) {
			helper.Out.x = helper.Out.y = helper.Out.z = 0;
		}
		else {
			helper.Out.x = helper.In.x;
			helper.Out.y = helper.In.y;
			helper.Out.z = helper.In.z;
		}
		switch ((int)mp_zxy) {
		case 0:
			helper.Out.z= v;
			break;
		case 1:
			helper.Out.x= v;
			break;
		default:
			helper.Out.y= v;
			break;
		}
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_zxy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_L0 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_An = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_Na = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_Bn = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_Nb = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_exp = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_E = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_log = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_G = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_C = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t in; \n"
		      "\t\t switch ((int)"<< mp_zxy <<") { \n"
		      "\t\t case 0: \n"
		      "\t\t\t in=vIn.z; \n"
		      "\t\t\t break; \n"
		      "\t\t case 1: \n"
		      "\t\t\t in=vIn.x; \n"
		      "\t\t\t break; \n"
		      "\t\t default: \n"
		      "\t\t\t in=vIn.y; \n"
		      "\t\t\t break; \n"
		      "\t\t } \n"
		      "\t\t real_t v="<< mp_L0 <<"*in+"<< mp_C <<"; \n"
		      "\t\t if( "<< mp_E <<" !=0) \n"
		      "\t\t\t v+="<< mp_exp <<"*exp("<< mp_E <<"*in) - 1; \n"
		      "\t\t if( "<< mp_An <<" !=0) \n"
		      "\t\t\t v+="<< mp_An <<" * ("<< mp_Na <<"==0? 1: ( "<< mp_Na <<"==1 ? in : (in<=0? 0: pow(in , "<< mp_Na <<")) )); \n"
		      "\t\t if( "<< mp_Bn <<" !=0) \n"
		      "\t\t\t v+="<< mp_Bn <<" * ("<< mp_Nb <<"==0? 1: ( "<< mp_Nb <<"==1 ? in : (in<=0? 0: pow(in , "<< mp_Nb <<")) )); \n"
		      "\t\t if( "<< mp_log <<" !=0) \n"
		      "\t\t\t v+="<< mp_log <<" * ( "<< mp_G <<" * in<=0? 0: log("<< mp_G <<"*in+1)); \n"
		      "\t\t v = "<< weight <<" * v; \n";
		if(m_VarType == eVariationType::VARTYPE_REG) { 
		ss << "\t\t\t vOut.x = vOut.y = vOut.z = 0; \n";
		} 
		else { 
		ss << "\t\t\t vOut.x = vIn.x; \n"
		      "\t\t\t vOut.y = vIn.y; \n"
		      "\t\t\t vOut.z = vIn.z; \n";
		} 
		ss << "\t\t switch ((int)"<< mp_zxy <<") { \n"
		      "\t\t case 0: \n"
		      "\t\t\t vOut.z= v; \n"
		      "\t\t\t break; \n"
		      "\t\t case 1: \n"
		      "\t\t\t vOut.x= v; \n"
		      "\t\t\t break; \n"
		      "\t\t default: \n"
		      "\t\t\t vOut.y= v; \n"
		      "\t\t\t break; \n"
		      "\t\t } \n";
		
		ss << "\t }\n";
		return ss.str();
	}

protected:
    void Init() {
        string prefix = Prefix()+"zmod""_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_zxy, prefix + "zxy",0,eParamType::INTEGER,0,2));
        m_Params.push_back(ParamWithName<T>(&mp_L0, prefix + "L0",1));
        m_Params.push_back(ParamWithName<T>(&mp_An, prefix + "An",0));
        m_Params.push_back(ParamWithName<T>(&mp_Na, prefix + "Na",0));
        m_Params.push_back(ParamWithName<T>(&mp_Bn, prefix + "Bn",0));
        m_Params.push_back(ParamWithName<T>(&mp_Nb, prefix + "Nb",0));
        m_Params.push_back(ParamWithName<T>(&mp_exp, prefix + "exp",1));
        m_Params.push_back(ParamWithName<T>(&mp_E, prefix + "E",0));
        m_Params.push_back(ParamWithName<T>(&mp_log, prefix + "log",0));
        m_Params.push_back(ParamWithName<T>(&mp_G, prefix + "G",1));
        m_Params.push_back(ParamWithName<T>(&mp_C, prefix + "C",0));
    }

private:
    T mp_zxy;
    T mp_L0;
    T mp_An;
	T mp_Na;
	T mp_Bn;
	T mp_Nb;
    T mp_exp;
    T mp_E;
    T mp_log;
    T mp_G;
    T mp_C;
};
MAKEPREPOSTPLUGINPARVAR(ZXYMod, zxymod) ;

/// <summary>
/// Rotate Center.
/// </summary>
/// varFullName : RotateCenter
/// stringName  : rot_center
template <typename T>
class RotateCenter : public ParametricVariation<T>
{
public:
	RotateCenter(T weight = 1.0) : ParametricVariation<T>("rot_center", DUMMYVARID, weight)
	{
		Init();
	}

	PARVARCOPY(RotateCenter)

    
    virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
    {
        T dx=helper.In.x-mp_x;
        T dy=helper.In.y-mp_y;
        T ang = std::atan2(dy,dx) + pc_rot_rad;
        T rad = VarFuncs<T>::Hypot(dx,dy)*mp_rscale;

        helper.Out.x = m_Weight * ( mp_x + rad*std::cos(ang) );
        helper.Out.y = m_Weight * ( mp_y + rad*std::sin(ang) );
        helper.Out.z = m_Weight * helper.In.z;
        
    }

	virtual string OpenCLString() const override
    {
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_ang = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rscale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_x = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_y = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_rot_rad = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t dx=vIn.x-"<< mp_x <<"; \n"
		      "\t\t real_t dy=vIn.y-"<< mp_y <<"; \n"
		      "\t\t real_t ang = atan2(dy,dx) + "<< pc_rot_rad <<"; \n"
		      "\t\t real_t rad = Hypot(dx,dy)*"<< mp_rscale <<"; \n"
		      "\t\t vOut.x = "<< weight <<" * ( "<< mp_x <<" + rad*cos(ang) ); \n"
		      "\t\t vOut.y = "<< weight <<" * ( "<< mp_y <<" + rad*sin(ang) ); \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

	virtual void Precalc() override
    {
        pc_rot_rad=mp_ang*T(2)*T(M_PI);
    }

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> {  "Hypot" };
	}

protected:
    void Init() {
        string prefix = Prefix()+"rot_center" "_";
        m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_ang, prefix + "ang",0));
        m_Params.push_back(ParamWithName<T>(&mp_rscale, prefix + "rscale",1));
        m_Params.push_back(ParamWithName<T>(&mp_x, prefix + "x",0));
        m_Params.push_back(ParamWithName<T>(&mp_y, prefix + "y",0));
        m_Params.push_back(ParamWithName<T>(true,&pc_rot_rad, prefix + "rot_rad"));
    }

private:


    T mp_ang;
    T mp_rscale;
    T mp_x;
    T mp_y;
    T pc_rot_rad;
};
MAKEPREPOSTPLUGINPARVAR(RotateCenter, rot_center) ;


#define VAR_COUNT 60

Variation<float> * variationsFloat[VAR_COUNT];
Variation<double> * variationsDouble[VAR_COUNT];
VariationPluginInfo variationsInfo={0, variationsFloat,variationsDouble};

void initializeVariationList() {

    if(variationsInfo.size == 0) {
        int i=0;
        
        POPULATEVARLIST_REGPREPOST(BlurRing) ;
        POPULATEVARLIST_REGPREPOST(FastGrid) ;
        POPULATEVARLIST_REGPREPOST(Kage) ;
        POPULATEVARLIST_REGPREPOST(KageAng) ;
        POPULATEVARLIST_REGPREPOST(KageAng2) ;
        POPULATEVARLIST_REGPREPOST(KageAng2C) ;
        POPULATEVARLIST_REGPREPOST(LinearLt) ;
        POPULATEVARLIST_REGPREPOST(BlurZ2) ;
        POPULATEVARLIST_REGPREPOST(BlurZTail) ;
        POPULATEVARLIST_REGPREPOST(ZXYMod) ;
        POPULATEVARLIST_REGPREPOST(RotateCenter) ;
        
        variationsInfo.size=i;
    }
}

}

/// <summary>
/// Return Info of Variations contained in this plugin:
///   size and variation lists
/// </summary>
extern "C" 
VARPLUGINEXP VariationPluginInfo*  GetVariationList() {
    EmberNs::initializeVariationList();
    return & EmberNs::variationsInfo ;
}



