﻿#include "Variation.h"
#include "XForm.h"
#include "VariationPluginInfo.h"
#include "../EmberPluginMacro.h"
#include "../DllExport.h"


namespace EmberNs {

/// <summary>
/// Cpow Real.
/// </summary>
/// varFullName : CpowReal
/// stringName  : cpow_real
template <typename T>
class CpowReal : public ParametricVariation<T>
{
public:
	CpowReal(T weight = 1.0) : ParametricVariation<T>("cpow_real", DUMMYVARID, weight, true, false, false, false, true)
	{
		Init();
	}

    PARVARCOPY(CpowReal)


	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
        T a = helper.m_PrecalcAtanyx;
		T r = std::sqrt( helper.m_PrecalcSumSquares );
        if (r>mp_bailout)
        {
            helper.Out.x=rand.Frand01<T>();
            helper.Out.y=rand.Frand01<T>();
        }
        else
        {
            if (rand.RandBit())
                a += 2*T(M_PI);
            //        T k2pi=
            //		T angle = pc_C * a + pc_D * lnr + pc_Ang * Floor<T>(mp_Power * rand.Frand01<T>());
            //        T angle = mp_Power*(a + Floor<T>(10 * rand.Frand01<T>()-5)*2* T(M_PI));
            T angle = mp_Power*a ;
            T m = m_Weight * mp_Power * r ;
            helper.Out.x = m * std::cos(angle)+mp_R;
            helper.Out.y = m * std::sin(angle)+mp_I;
            //        std::complex<T> z(helper.In.x, helper.In.y);
            //		z = T(1.0) / z;
            //        std::complex<T> result = std::exp(mp_Power* std::log(z));
            //        helper.Out.x = result.real()+mp_R;
            //        helper.Out.y = result.imag();
        }
		helper.Out.z = DefaultZ(helper);	
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
//		string mp_in = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
//		string mp_out = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
//		string mp_start = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
//		string mp_end = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
//		ss << "\t {\n";
//		ss << "\t\t real_t rad = sqrt( "<< mp_start <<" + MwcNext01(mwc) * ("<< mp_end <<"-"<< mp_start <<") ); \n"
//		      "\t\t real_t temp = MwcNext01(mwc) * (real_t)(M_2PI); \n";
//		if(m_VarType == eVariationType::VARTYPE_POST) { 
//		ss << "\t\t\t vOut.x = "<< weight <<" * cos(temp) * rad + vIn.x; \n"
//		      "\t\t\t vOut.y = "<< weight <<" * sin(temp) * rad + vIn.y; \n"
//		      "\t\t\t vOut.z = vIn.z; \n";
//		} 
//		else { 
//		ss << "\t\t\t vOut.x = "<< weight <<" * cos(temp) * rad; \n"
//		      "\t\t\t vOut.y = "<< weight <<" * sin(temp) * rad; \n"
//		      "\t\t\t vOut.z = 0; \n";
//		} 
		
//		ss << "\t }\n";
		return ss.str();
    }

    virtual void Precalc() override
	{
		pc_C = mp_R * mp_Power;
		pc_D = mp_I * mp_Power;
		pc_Ang = 2 * T(M_PI) * mp_Power;
	}

//	virtual void Random(QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
//	{
//		mp_R = 3 * rand.Frand01<T>();
//		mp_I = rand.Frand01<T>() - T(0.5);
//		m_Params[2].Set(5 * rand.Frand01<T>());//Power.
//	}

protected:
	void Init()
	{
		string prefix = Prefix()+"cpow_real" "_";
		m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_R, prefix + "r", 1));//Params.
		m_Params.push_back(ParamWithName<T>(&mp_I, prefix + "i"));
		m_Params.push_back(ParamWithName<T>(&mp_Power, prefix + "power", 1));
        m_Params.push_back(ParamWithName<T>(&mp_bailout, prefix + "bailout", 4));
		m_Params.push_back(ParamWithName<T>(true, &pc_C, prefix + "c"));//Precalc.
		m_Params.push_back(ParamWithName<T>(true, &pc_D, prefix + "d"));
		m_Params.push_back(ParamWithName<T>(true, &pc_Ang, prefix + "ang"));
	}

private:
    T mp_R;//Params.
	T mp_I;
	T mp_Power;
    T mp_bailout;
	T pc_C;//Precalc.
	T pc_D;
	T pc_Ang;
};

//MAKEPREPOSTPLUGINPARVAR(CpowReal, cpow_real) ;

/// <summary>
/// Apollony.
/// z=(D-R)+R^2/(R+D-z)
/// </summary>
/// varFullName : Apollony
/// stringName  : apollony
template <typename T>
class Apollony : public ParametricVariation<T>
{
public:
	Apollony(T weight = 1.0) : ParametricVariation<T>("apollony", DUMMYVARID, weight)
	{
		Init();
	}

    PARVARCOPY(Apollony)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{        
        T z_r=mp_R+mp_D-helper.In.x;
        T z_i=helper.In.y;
        T r2 = T(1.0)/ Zeps(z_r*z_r + z_i*z_i);
        z_r = mp_D-mp_R + mp_R * mp_R * z_r * r2 ;
        z_i = mp_R * mp_R * z_i * r2 ;
//        if(mp_diVD) {
//            z_r *= pc_1D;
//            z_i *= pc_1D;
//        }
        if(mp_rotN != T(0)) {
            T n= Round( rand.Frand01<T>()* mp_rotN - T(0.5) ) - ((int)mp_rotN)/2;
            T ang=atan2(z_i,z_r) + n * pc_angN;
            T rad = m_Weight * VarFuncs<T>::Hypot(z_r,z_i);
            helper.Out.x = rad * std::cos(ang) ;
            helper.Out.y = rad * std::sin(ang) ;
        } else {
            helper.Out.x = m_Weight * z_r;
            helper.Out.y = m_Weight * z_i;
        }
		helper.Out.z = m_Weight * helper.In.z;	
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_R = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_D = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_rotN = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_R2 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_angN = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t z_r="<< mp_R <<"+"<< mp_D <<"-vIn.x; \n"
		      "\t\t real_t z_i=vIn.y; \n"
		      "\t\t real_t r2 = (real_t)(1.0)/ Zeps(z_r*z_r + z_i*z_i); \n"
		      "\t\t z_r = "<< mp_D <<"-"<< mp_R <<" + "<< mp_R <<" * "<< mp_R <<" * z_r * r2 ; \n"
		      "\t\t z_i = "<< mp_R <<" * "<< mp_R <<" * z_i * r2 ; \n"
		      "\t\t if("<< mp_rotN <<" != (real_t)(0)) { \n"
		      "\t\t\t real_t n= Round( MwcNext01(mwc)* "<< mp_rotN <<" - (real_t)(0.5) ) - ((int)"<< mp_rotN <<")/2; \n"
		      "\t\t\t real_t ang=atan2(z_i,z_r) + n * "<< pc_angN <<"; \n"
		      "\t\t\t real_t rad = "<< weight <<" * Hypot(z_r,z_i); \n"
		      "\t\t\t vOut.x = rad * cos(ang) ; \n"
		      "\t\t\t vOut.y = rad * sin(ang) ; \n"
		      "\t\t } else { \n"
		      "\t\t\t vOut.x = "<< weight <<" * z_r; \n"
		      "\t\t\t vOut.y = "<< weight <<" * z_i; \n"
		      "\t\t } \n"
		      "\t\t vOut.z = "<< weight <<" * vIn.z; \n";
		
		ss << "\t }\n";
		return ss.str();
    }

    virtual void Precalc() override
	{
        pc_R2 = mp_R * mp_R;
		pc_angN = T(M_PI)*2 / mp_rotN;
//        pc_1D = mp_D==T(0) ? T(0) : T(1.0)/mp_D;
	}
    
    virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round", "Hypot", "Zeps" };
	}
    
protected:
	void Init()
	{
		string prefix = Prefix()+"apollony" "_";
		m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_R, prefix + "r", T(1.7320508075688772935274463415059) ));//sqrt(3)
		m_Params.push_back(ParamWithName<T>(&mp_D, prefix + "d",1));
        m_Params.push_back(ParamWithName<T>(&mp_rotN, prefix + "rotN",0,eParamType::INTEGER,0));
//        m_Params.push_back(ParamWithName<T>(&mp_diVD, prefix + "divD",0,eParamType::INTEGER,0,1));
        m_Params.push_back(ParamWithName<T>(true, &pc_R2, prefix + "r2"));
        m_Params.push_back(ParamWithName<T>(true, &pc_angN, prefix + "angN"));
//		m_Params.push_back(ParamWithName<T>(true, &pc_1D, prefix + "1d"));

	}

private:
    T mp_R;
    T mp_D;
    T mp_rotN;
//    T mp_diVD;
    T pc_R2;
    T pc_angN;
//    T pc_1D;

};

MAKEPREPOSTPLUGINPARVAR(Apollony, apollony) ;



/// <summary>
/// RMod.
/// </summary>
/// varFullName : RMod
/// stringName  : rmod
template <typename T>
class RMod : public ParametricVariation<T>
{
public:
	RMod(T weight = 1.0) : ParametricVariation<T>("rmod", DUMMYVARID, weight, true, false, false, false, true)
    {
		Init();
	}

    PARVARCOPY(RMod)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
        T atan = helper.m_PrecalcAtanyx;
        T rad = helper.m_PrecalcSumSquares + mp_Ar2;
		rad = (std::sqrt( rad> T(0) ? rad : T(0) )+ mp_Br) * mp_scale;
        helper.Out.x = m_Weight*( rad * std::cos(atan) + mp_zX ) ;
        helper.Out.y = m_Weight*( rad * std::sin(atan) + mp_zY ) ;
		helper.Out.z = m_Weight * DefaultZ(helper);	
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_Ar2 = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_Br = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_scale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_zX = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_zY = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t atan = precalcAtanyx; \n"
		      "\t\t real_t rad = precalcSumSquares + "<< mp_Ar2 <<"; \n"
		      "\t\t rad = (sqrt( rad> (real_t)(0) ? rad : (real_t)(0) )+ "<< mp_Br <<") * "<< mp_scale <<"; \n"
		      "\t\t vOut.x = "<< weight <<"*( rad * cos(atan) + "<< mp_zX <<" ) ; \n"
		      "\t\t vOut.y = "<< weight <<"*( rad * sin(atan) + "<< mp_zY <<" ) ; \n"
		      "\t\t vOut.z = "<< weight <<" * " << DefaultZCl();
		
		ss << "\t }\n";
		return ss.str();
    }

    
protected:
	void Init()
	{
		string prefix = Prefix()+"rmod" "_";
		m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_Ar2, prefix + "Ar2", 0 ));
		m_Params.push_back(ParamWithName<T>(&mp_Br, prefix + "Br",0));
        m_Params.push_back(ParamWithName<T>(&mp_scale, prefix + "scale",1));
        m_Params.push_back(ParamWithName<T>(&mp_zX, prefix + "zX",0));
        m_Params.push_back(ParamWithName<T>(&mp_zY, prefix + "zY",0));

	}

private:
    T mp_Ar2;
    T mp_Br;
    T mp_scale;
    T mp_zX;
    T mp_zY;

};

MAKEPREPOSTPLUGINPARVAR(RMod, rmod) ;

/// <summary>
/// BubbleFix.
/// Slight modification of Bubble to get good 3D effect
/// </summary>
/// varFullName : BubbleFix
/// stringName  : bubbleFix
template <typename T>
class BubbleFix : public Variation<T>
{
public:
	BubbleFix(T weight = 1.0) : Variation<T>("bubbleFix", DUMMYVARID, weight, true) { }

	VARCOPY(BubbleFix)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
		T _1_denom_ = T(1.0) / (T(0.25) * helper.m_PrecalcSumSquares + T(1.0));
		T r = m_Weight * _1_denom_;
		helper.Out.x = r * helper.In.x;
		helper.Out.y = r * helper.In.y;
		helper.Out.z = m_Weight * (T(2.0) * _1_denom_ - T(1.0) );
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t varIndex = IndexInXform();
		
		ss << "\t {\n";
		ss << "\t\t real_t _1_denom_ = (real_t)(1.0) / ((real_t)(0.25) * precalcSumSquares + (real_t)(1.0)); \n"
		      "\t\t real_t r = "<< weight <<" * _1_denom_; \n"
		      "\t\t vOut.x = r * vIn.x; \n"
		      "\t\t vOut.y = r * vIn.y; \n"
		      "\t\t vOut.z = "<< weight <<" * ((real_t)(2.0) * _1_denom_ - (real_t)(1.0) ); \n";
		
		ss << "\t }\n";
		return ss.str();
	}

};
MAKEPREPOSTPLUGINVAR(BubbleFix, bubbleFix) ;

/// <summary>
/// DC Bubble Fix.
/// </summary>
/// varFullName : DCBubbleFix
/// stringName  : dc_bubbleFix
template <typename T>
class DCBubbleFix : public ParametricVariation<T>
{
public:
	DCBubbleFix(T weight = 1.0) : ParametricVariation<T>("dc_bubbleFix", DUMMYVARID, weight, true)
	{
		Init();
	}

	PARVARCOPY(DCBubbleFix)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
        T _1_denom_ = T(1.0) / (T(0.25) * helper.m_PrecalcSumSquares + T(1.0));
		T r = m_Weight * _1_denom_;
		helper.Out.x = r * helper.In.x;
		helper.Out.y = r * helper.In.y;
		helper.Out.z = m_Weight * (T(2.0) * _1_denom_ - T(1.0) );
        
		T sumX, sumY;

		if (m_VarType == eVariationType::VARTYPE_PRE)
		{
			sumX = helper.In.x;
			sumY = helper.In.y;
		}
		else
		{
			sumX = outPoint.m_X;
			sumY = outPoint.m_Y;
		}

		T tempX = helper.Out.x + sumX;
		T tempY = helper.Out.y + sumY;
		outPoint.m_ColorX = fmod(std::abs(pc_Bdcs * (Sqr<T>(tempX + mp_CenterX) + Sqr<T>(tempY + mp_CenterY))), T(1.0));
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_CenterX = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_CenterY = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_Scale = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_Bdcs = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t _1_denom_ = (real_t)(1.0) / ((real_t)(0.25) * precalcSumSquares + (real_t)(1.0)); \n"
		      "\t\t real_t r = "<< weight <<" * _1_denom_; \n"
		      "\t\t vOut.x = r * vIn.x; \n"
		      "\t\t vOut.y = r * vIn.y; \n"
		      "\t\t vOut.z = "<< weight <<" * ((real_t)(2.0) * _1_denom_ - (real_t)(1.0) ); \n"
		      "\t\t real_t sumX, sumY; \n";
		if (m_VarType == eVariationType::VARTYPE_PRE) 
		{ 
		ss << "\t\t\t sumX = vIn.x; \n"
		      "\t\t\t sumY = vIn.y; \n";
		} 
		else 
		{ 
		ss << "\t\t\t sumX = outPoint->m_X; \n"
		      "\t\t\t sumY = outPoint->m_Y; \n";
		} 
		ss << "\t\t real_t tempX = vOut.x + sumX; \n"
		      "\t\t real_t tempY = vOut.y + sumY; \n"
		      "\t\t outPoint->m_ColorX = fmod(fabs("<< pc_Bdcs <<" * (Sqr(tempX + "<< mp_CenterX <<") + Sqr(tempY + "<< mp_CenterY <<"))), (real_t)(1.0)); \n";
		
		ss << "\t }\n";
		return ss.str();
	}

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Sqr" };
	}

	virtual void Precalc() override
	{
		pc_Bdcs = 1 / (mp_Scale == 0 ? T(10E-6) : mp_Scale);
	}

protected:
	void Init()
	{
		string prefix = Prefix() +"dc_bubbleFix" "_";
		m_Params.clear();
		m_Params.push_back(ParamWithName<T>(&mp_CenterX, prefix + "centerx"));//Params.
		m_Params.push_back(ParamWithName<T>(&mp_CenterY, prefix + "centery"));
		m_Params.push_back(ParamWithName<T>(&mp_Scale, prefix + "scale", 1));
		m_Params.push_back(ParamWithName<T>(true, &pc_Bdcs, prefix + "bdcs"));//Precalc.
	}

private:
	T mp_CenterX;//Params.
	T mp_CenterY;
	T mp_Scale;
	T pc_Bdcs;//Precalc.
};
MAKEPREPOSTPLUGINPARVAR(DCBubbleFix, dc_bubbleFix) ;



/// <summary>
/// TileLog2.
/// </summary>
template <typename T>
class TileLog2 : public ParametricVariation<T>
{
public:
	TileLog2(T weight = 1.0) : ParametricVariation<T>("tile_log2", eVariationId::VAR_TILE_LOG, weight)
	{
		Init();
	}

	PARVARCOPY(TileLog2)

	virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
	{
		T temp = Round(std::log(rand.Frand01<T>()) * (rand.Rand() & 1 ? mp_spread : -mp_spread));
		helper.Out.x = m_Weight * (helper.In.x + temp * pc_dx);
		helper.Out.y = m_Weight * (helper.In.y + temp * pc_dy);
		helper.Out.z = DefaultZ(helper);
	}

	virtual string OpenCLString() const override
	{
        ostringstream ss;
		string weight = WeightDefineString();
		intmax_t i = 0, varIndex = IndexInXform();
		string stateIndex = "_" + to_string(XformIndexInEmber());
		string index = stateIndex + "]";
		
		string mp_dist = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_ang = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string mp_spread = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_dx = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		string pc_dy = "parVars[" + ToUpper(m_Params[i++].Name()) + index;
		
		ss << "\t {\n";
		ss << "\t\t real_t temp = Round(log(MwcNext01(mwc)) * (MwcNext(mwc) & 1 ? "<< mp_spread <<" : -"<< mp_spread <<")); \n"
		      "\t\t vOut.x = "<< weight <<" * (vIn.x + temp * "<< pc_dx <<"); \n"
		      "\t\t vOut.y = "<< weight <<" * (vIn.y + temp * "<< pc_dy <<"); \n"
		      "\t\t vOut.z = " << DefaultZCl();
		
		ss << "\t }\n";
		return ss.str();
	}

	virtual vector<string> OpenCLGlobalFuncNames() const override
	{
		return vector<string> { "Round" };
	}
    
    virtual void Precalc() override
	{
        pc_dx=mp_dist*cos(mp_ang*T(M_PI));
        pc_dy=mp_dist*sin(mp_ang*T(M_PI));
	}

protected:
	void Init()
	{
		string prefix = Prefix()+ "tile_log2" "_";
		m_Params.clear();
        m_Params.push_back(ParamWithName<T>(&mp_dist, prefix + "dist", 1));
        m_Params.push_back(ParamWithName<T>(&mp_ang, prefix + "ang", 0));
		m_Params.push_back(ParamWithName<T>(&mp_spread, prefix + "spread", 1));
        m_Params.push_back(ParamWithName<T>(true,&pc_dx, prefix + "dx"));
        m_Params.push_back(ParamWithName<T>(true,&pc_dy, prefix + "dy"));
	}

private:
    T mp_dist;
    T mp_ang;
	T mp_spread;
    T pc_dx;
    T pc_dy;
};
MAKEPREPOSTPLUGINPARVAR(TileLog2, tile_log2) ;

#define VAR_COUNT 60

Variation<float> * variationsFloat[VAR_COUNT];
Variation<double> * variationsDouble[VAR_COUNT];
VariationPluginInfo variationsInfo={0, variationsFloat,variationsDouble};

void initializeVariationList() {

    if(variationsInfo.size == 0) {
        int i=0;
        
//        POPULATEVARLIST_REGPREPOST(CpowReal) ;
        POPULATEVARLIST_REGPREPOST(Apollony) ;
        POPULATEVARLIST_REGPREPOST(RMod) ;
        POPULATEVARLIST_REGPREPOST(BubbleFix) ;
        POPULATEVARLIST_REGPREPOST(DCBubbleFix) ;
        POPULATEVARLIST_REGPREPOST(TileLog2) ;


        
        variationsInfo.size=i;
    }
}

}

/// <summary>
/// Return Info of Variations contained in this plugin:
///   size and variation lists
/// </summary>
extern "C" 
VARPLUGINEXP VariationPluginInfo*  GetVariationList() {
    EmberNs::initializeVariationList();
    return & EmberNs::variationsInfo ;
}



