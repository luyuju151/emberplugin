TEMPLATE = lib
CONFIG += plugin
CONFIG += shared
CONFIG -= app_bundle
CONFIG -= qt

# folder and complied plugin file name of this plugin

EMBER_PLUGIN = LuPlugin03

TARGET = $$EMBER_PLUGIN

include(./../../pluginDefaults.pri)

PRJ_SRC_DIR = $$absolute_path(./../)

#message(PRJ_SRC_DIR: $$PRJ_SRC_DIR)

#Project specific compiler flags.

win32 {
    DEFINES += BUILDING_EMBERPLUGIN
}

!macx:PRECOMPILED_HEADER = $$PRJ_SRC_DIR/EmberPluginPch.h

SOURCES += \
    $$PRJ_SRC_DIR/$$EMBER_PLUGIN/$$basename(EMBER_PLUGIN).cpp

HEADERS += \
    $$PRJ_SRC_DIR/EmberPluginPch.h \
    $$PRJ_SRC_DIR/EmberPluginMacro.h \
    $$PRJ_SRC_DIR/DLLExport.h

