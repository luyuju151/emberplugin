#!/opt/bin/ruby
require 'fileutils'
require 'optparse'
require 'ostruct'
# require 'to_regexp'
require 'set'
# require 'io/console'

@@FUNC_REPLACE_LIST= [
	[ /(?<=\t| |^)T(?=\*| )/ , "real_t" ],
	[ "helper.In.x" , "vIn.x" ],
	[ "helper.In.y" , "vIn.y" ],
	[ "helper.In.z" , "vIn.z" ],
	[ "helper.Out.x" , "vOut.x" ],
	[ "helper.Out.y" , "vOut.y" ],
	[ "helper.Out.z" , "vOut.z" ],
	[ /\bhelper\s*\.\s*m_Precalc/ , "precalc" ],
	[ "Floor<T>" , "floor" ],
	[ /\bClamp\b/ , "clamp" ],
	[ "std::abs" , "fabs" ],
	[ /\babs\b/ , "fabs" ],
	[ "m_Weight" , "\"<< weight <<\"" ],
	[ "std::" , "" ],
	[ "rand.Frand01<T>()" , "MwcNext01(mwc)" ],
	[ "rand.Rand()" , "MwcNext(mwc)" ],
	[ "VarFuncs<T>::" , "" ],
	[ "outPoint.m_" , "outPoint->m_" ],
	[ /(?<=\b)T\s*\(/ , "(real_t)(" ],
	[ "<T>" , "" ],
]

@@FUNC_POST_REPLACE_LIST= [
	[ /\bDefaultZ\s*\(\s*helper\s*\)\s*;\s*\\n\s*"/ , '" << DefaultZCl()' ],
	[ /(\bDefaultZCl\(\))(\s*")/m , '\1 <<\2' ],
]

class ConvOpenCL

	@@outfile="__cl_string.cpp"

	# @@INVLID_CHAR_E=@@INVLID_CHAR.gsub('/','')
	@invalid_regexp
	@invalid_replacement
	@options
	@result
	@escape_mark
	@tabbed_space
	@global_funcs
	def initialize
		@result=OpenStruct.new
		@escape_mark="````````"
		@tabbed_space=" "
		@global_funcs=[]
  end

	def parse_arg (args)
		options=OpenStruct.new


		@options=options

    opt_parser = OptionParser.new do |opts|
			opts.banner = "Generate OpenCLString from func() of fractorium plugin cpp.\n
Usage: conv-opencl.rb -p PLUGIN_NAME -v VARIATION_NAME \n
    or conv-opencl.rb -p PLUGIN_NAME \n
		   will return list of variations in PLUGINNAME.cpp, enter number to generate.\n
    or conv-opencl.rb -p PLUGIN_NAME -v all \n
    or conv-opencl.rb -p PLUGIN_NAME -n3 \n"


      opts.separator ""
      opts.separator "Specific options:"

			options.plugin = ""
			opts.on("-p", "--plugin PLUGIN_NAME",
              "plugin (folder) name contents the cpp.\n
							 Folder structure: PLUGINNAME\\PLUGINNAME.cpp") do |s|
				options.plugin = s
      end

			options.variation = ""
			opts.on("-v", "--variation VARIATION_NAME",
              "class name of the variation.") do |s|
				options.variation = s.strip
      end

			options.number = nil
			opts.on("-n", "--number N",OptionParser::DecimalInteger,
              "select N st variation, starting from 0.\n
							negative N means count from end end (-1 is last).") do |n|
				options.number = n
      end

			options.continuous = false
			opts.on("-c", "--continuous",
              "OpenCl string are continuous, try to use\n
							<<\"foo\" \"bar\"  instead of  <<\"foo\" << \"bar\" ") do
				options.continuous = true
      end

			options.include_comment = false
			opts.on("-I", "--include-comment",
							"Do not ignore inline comment when paring //.no.recommend..") do
				options.include_comment = true
			end

			options.tabsize = 4
			opts.on("-t", "--tabsize N",OptionParser::DecimalInteger,
							"number of whitespaces per tab \\t.") do |n|
				options.tabsize = n.clamp(0, 20)
			end
			@tabbed_space=" " * options.tabsize

			options.verbose=false
      opts.on("-V", "--verbose",
				 			"Verbose") do
        options.verbose=true
      end

			options.debug=false
      opts.on("-D", "--debug",
				 			"Very verbose") do
        options.verbose=true
        options.debug=true
      end

      opts.separator ""
      opts.separator "Common options:"

      # No argument, shows at tail.  This will print an options summary.
      opts.on_tail("-h", "--help", "Show this message") do
        puts opts
        exit
      end

    end
		puts opt_parser if args.length==0
    opt_parser.parse(args)
    options
  end

	def output str
		puts str
	end

	def log str
		output str if @options.verbose
	end

	def debug str
		output str if @options.debug
	end

	def warn str
		output "!!!  "+str
	end

	def summary str
		output str
	end

	def process (args)

		@options=self.parse_arg args
		log "argCount: #{args.length}"
		log @options
		if @options.plugin.empty?
			summary "Plugin name empty, exiting .."
			return false
		end
		results=load_cpp_file @options.plugin
		if(results.empty?)
			summary "\nNo variation found.\n"
		end

# puts -6%7
# return

		selected=[]
		if(! @options.variation.empty?)
			if (@options.variation.downcase =="all")
				selected=results
			else
				results.each do |result|
					if(@options.variation.upcase == result.varclassname.upcase || @options.variation.upcase == result.varstringname.upcase)
						selected << result
						break
					end
				end
			end
		elsif (! @options.number.nil?)
			n=@options.number.clamp(-results.size,results.size) % results.size
			summary "select #{n}/0~#{results.size-1}"
			selected << results[n]
		else
			summary "\nPlease input the number, or type 'all' :"
			s = STDIN.gets.strip
			n=s.to_i
			if(s=="all")
				n=nil
				summary "select all"
				selected = results
				# process=true
			end
			if(n)
				n=n.clamp(-results.size,results.size) % results.size
				summary "select #{n}/0~#{results.size-1}"
				selected << results[n]
			else
				warn "Number invalid"
			end
		end


		if(selected.empty?)
			summary "no variation processed, exit..."
			return false
		end
		processed=[]
		selected.each do |result|
			str = parse_var(result )
			processed << str if str
		end

		header=""
		header << '#include "Variation.h"' + "\r\n"
		header << '#include "XForm.h"' + "\r\n"
		header << '#include "VariationPluginInfo.h"' + "\r\n"
		header << '#include "EmberPluginMacro.h"' + "\r\n"
		header << '#include "DllExport.h"' + "\r\n"
		header << "\r\n"
		header << "\r\n"
		header << "namespace EmberNs {\r\n"
		header << "\r\n"

		tail = ""
		tail << "\r\n"
		tail << "}\r\n" # end of namespace EmberNs
		tail << "\r\n"


		if ! processed.empty?
			content=""
			content << header
			content << processed.join("\r\n")
			content << tail
			write_to_file content
			summary "\n       --  Finished.  --  \n"
		end
		# `pause`
		# return
	end

	def write_to_file(string)
		# include headers for correct display in IDE
    File.open(@@outfile, "wb") { |f|
			f.write string
		}
	end

	def load_cpp_file(plugin)
		cppfile= File.expand_path(File.join(plugin,+plugin +".cpp") )
		log "cppfile: "+cppfile
		work cppfile
	end

	def work(filename)
		if(! File.exist?(filename))
			warn 'File does not exist:\n' + cppfile
			return false
		end
		file = File.open(filename, "rb:UTF-8:UTF-8")
		content = file.read
		file.close
		parse content
	end


	def parse(content)
		# found=[]
		# i=0
		 # isparvar = false
		 # pluginname = ""
		 # varclassname = ""

		class_start = -1
		class_end=-1
		# -1 : no the state
		# 0 : on going
		# 1 : is the state
		result=OpenStruct.new
		results=[]
		content.each_line.with_index do |line,index|
			line=line.gsub(/\/\/.*$/,"") unless @options.include_comment
			line_strip=line.tr(" \t\r\n", '')
			# puts "#{index}  : " +line
			next if (line_strip.start_with? ("//"))
			# class difiniation start
			if (line_strip.include? (":public"))
				# class ZXYMod : public Variation<T>
				if(line_strip.include? (":publicVariation<T>"))
					class_start = 0
					isparvar = false
				elsif(line_strip.include? (":publicParametricVariation<T>"))
					# class ZXYMod : public ParametricVariation<T>
					class_start = 0
					isparvar = true
				end

				if(class_start ==0)
					varname = line[/class\s+([_\w]+)[\s:]/, 1]
					if(varname)
						# prepare new class
						result=OpenStruct.new
						result.varclassname=varname
						result.varstringname=""
						result.str_arr=[]
						result.str_arr.push line
						result.isparvar=isparvar
						class_start=1
						class_end=-1
						log ("[%3d ]  "%results.size) + ("%-25s "%result.varclassname) +"@ #{index}"
						results.push result
					end
				end
			end
			# class difiniation end

			if(class_start == 1)
				result.str_arr.push line

				if(line_strip.include? ("ariation<T>(\""))
					# ZXYMod(T weight = 1.0) : ParametricVariation<T>("zxymod", DUMMYVARID, weight)
					stringname = line[/ariation\s*<\s*T\s*>\s*\(\s*"([^"]*)"/, 1]
					result.varstringname = stringname if(stringname)
				end

			end

			# detect end of class difiniation
			if (line.match (/^};\s*$/))
				#  line begin with   \};
				class_end=0
			end
			if (line_strip.include? ("MAKEPREPOSTPLUGINP"))
				# MAKEPREPOSTPLUGINPARVAR
				# MAKEPREPOSTPLUGINVAR
				class_end=0
			end
			if (line_strip.include? ("#defineVAR_COUNT"))
				# #define VAR_COUNT 60
				class_end=0
			end

			#perform end of class difiniation start
			if (class_start == 1 && class_end ==0)
				class_end=1
				class_start=-1
				log ("[%3d ]  "% (results.size-1)) + ("%-25s "%result.varclassname) +"@ #{index} --end"
				info result
			end
			#perform end of class difiniation end
		end
		summary "Total found vars: #{results.size}"
		results.each_with_index do |result,index|
			# log "============================================="
			summary ("[%3d ]  "%index) + ("%-25s "%result.varclassname) + " : "+result.varstringname
			# parseVar result
		end
		return results
	end


	def tidy(str)
		a,b=split_leading(str)
		a=space_to_tab(tab_to_space(a))
		b=trim_right(b).gsub(/[ \t]+/," ")
		return a+b
	end

	def tidy_all_space(str)
		a,b=split_leading(str)
		a=tab_to_space(a)
		b=trim_right(b).gsub(/[ \t]+/," ")
		return a+b
	end

	def trim_right!(str)
		return str.gsub!(/\s+$/,"")
	end

	def trim_right(str)
		return str.gsub(/\s+$/,"")
	end

	def split_leading(str)
		leading = str[/^\s*/, 0]
		leading="" if ! leading
		tailing=str[leading.size..-1]
		return [leading,tailing]
	end
	def tab_to_space(str)
		# a,b=split_leading(aa)
		# return a.gsub("\t"," "* @options.tabsize)+b
		return str.gsub("\t",@tabbed_space)
	end

	def tab_to_space_leading(str)
		a,b=split_leading(aa)
		return a.gsub("\t",@tabbed_space)+b
		# return str.gsub("\t"," "* @options.tabsize)
	end

	def space_to_tab(str)
		a,b=split_leading(str)
		return a.gsub(@tabbed_space,"\t")+b
	end

	def tab_to_t(str,sep="")
		a,b=split_leading(str)
		a.gsub!("\t","\\t")
		a<< sep if a.match /^(\\t)*$/
		return a+b
	end

	def info(result)
		return if ! @options.verbose
		summary result.varclassname + " : \"#{result.varstringname}\""
		summary "Parvar: #{result.isparvar}    Lines: #{result.str_arr.size}"
		summary "------------------------------------------------"
	end

	def parse_var result
		log "----------------------parse_var--------------------------"
		summary "parsing #{result.varclassname} : #{result.varstringname}"

		func_start=-10
		func_arr=[]
		para_str=[]
		para_start=-10

		bracket=0
		result.str_arr.each_with_index do |line,index|
			line=line.gsub(/\\\\.*$/,"")
			line_strip=line.tr(" \t\r\n", '')
			# puts "#{index}  : " +line
			# next if (line_strip.start_with? ("//"))
			next if (line_strip.empty?)

			# start of Func()
			if(line_strip.include? ("voidFunc(IteratorHelper<T>&"))
				# virtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override
				func_start=0
				bracket=0
			end

			if (func_start>=0)
					bracket+=line_strip.count("{")
					bracket-=line_strip.count("}")
					if(func_start==0 && bracket >0)
						func_start=1
					end
					#end of func()
					if(func_start==1 && bracket <= 0)
						func_start=-1
						if(line_strip == "}")
							next
						end
					end
					# in func() , record line
					func_arr.push line if(func_start==1)
			end

			if(func_start == -1)
				if(line_strip.start_with? ("private:"))
					# private:
					para_start=1
					next
				end
				if(para_start==1)
					if(line_strip.match /^\s*}/ )
						para_start=-1
						break
					end
					para_str.push line
				end
			end
		end

		log "#{result.varclassname} Func() lines : #{func_arr.size}"
		if(func_arr.size>0)
			func_arr[0].sub!(/^\s*{/,"")
			func_arr[0]="" if(func_arr[0].tr(" \t\r\n", '').size==0 )
			func_arr[func_arr.size-1]="" if(func_arr[func_arr.size-1].tr(" \t\r\n", '').size==0 )
		end
		func_str_origin=func_arr.join
		# summary func
		summary "----Paras----"
		# log para_str

		paras=extract_para para_str
		res_func, leading_spaces=conv_func(func_arr,paras)

		(warn "conv_func failed"; return false) if(! res_func)
		res_func=func_to_cl(res_func)

		(warn "func_to_cl failed"; return false) if(! res_func)


		# start building variation source code
		final=""
		comment="//"

		# class start
		inherit_var_string= result.isparvar ? "ParametricVariation" : "Variation"
		final << "/// <summary>\r\n";
		final << "/// " + result.varclassname + ".\r\n";
		final << "/// </summary>\r\n";
		final << "/// </summary>\r\n";
		final << "/// varFullName : " + result.varclassname + "\r\n";
		final << "/// stringName  : " + result.varstringname + "\r\n";
		final << "template <typename T>\r\n";
		final << "class " + result.varclassname + " : public "+inherit_var_string+"<T>\r\n";

		# constructor()
		final << "{\r\n";
		final << "public:\r\n";
		# final += "\t" + varclassname + "(T weight = 1.0) : ParametricVariation<T>(\"" + pluginname + "\", eVariationId::VAR_" + pluginname.ToUpper() + ", weight)\r\n";
		final << "\t" + result.varclassname + "(T weight = 1.0) : "+inherit_var_string+"<T>(\"" + result.varstringname + "\", DUMMYVARID, weight)\r\n";
		final << "\t{\r\n";
 		if (result.isparvar)
			final << "\t	Init();\r\n"
		end
		final << "\t}\r\n";
		final << "\r\n";

		# VARCOPY
		if (result.isparvar)
			final << "\tPARVARCOPY(" + result.varclassname + ")\r\n";
		else
			final << "\tVARCOPY(" + result.varclassname + ")\r\n";
		end

		# Func
		final << "\r\n";
		final << "\tvirtual void Func(IteratorHelper<T>& helper, Point<T>& outPoint, QTIsaac<ISAAC_SIZE, ISAAC_INT>& rand) override\r\n";
		final << "\t{\r\n";
		func_str=""
		func_arr.each do |line|
			func_str << space_to_tab(line)
		end
		final << func_str
		final << "\t}\r\n";

		# OpenCLString()
		final << "\r\n";
		final << "\tvirtual string OpenCLString() const override\r\n";
		final << "\t{\r\n";
		final << "\t	ostringstream ss;\r\n";
		final << "\t	string weight = WeightDefineString();\r\n";
		if(! result.isparvar)
			#  Variation<T>
			final << "\t	intmax_t varIndex = IndexInXform();\r\n";
		else
			#  ParametricVariation<T>
			final << "\t	intmax_t i = 0, varIndex = IndexInXform();\r\n";
			final << "\t	string stateIndex = \"_\" + to_string(XformIndexInEmber());\r\n";
			final << "\t	string index = stateIndex + \"]\";\r\n";
			final << "\t	\r\n";
			paras.each do |param|
				if(param.type==ParaType::T)
					final << "\t	string " + param.name + " = \"parVars[\" + ToUpper(m_Params[i++].Name()) + index;\r\n";
				elsif (param.type==ParaType::ARRAY)
					#string rand = ToUpper(m_Params[i].Name()) + stateIndex; i += COUNT;
					final << "\t	string " + param.name + " = ToUpper(m_Params[i].Name()) + stateIndex; i+=" + param.arr_length_str + ";\r\n";
				end
			end
		end
		final << "\t	\r\n";
		final << "\t	ss << \"\\t {\\n\";\r\n";
		prefix="\t	"
		res_func.each_line do |line|
			final << prefix<< line.gsub("\n","\r\n")
		end
		final << "\t	\r\n";
		final << "\t	ss << \"\\t }\\n\";\r\n";
		final << "\t	return ss.str();\r\n";
		final << "\t}\r\n";

		# Precalc()
		if (result.isparvar)
			has_pc=false
			paras.each do |param|
				(has_pc=true;break) if param.isprecalc
			end
			if has_pc
				final << "\r\n";
				final << comment << "\tvirtual void Precalc() override\r\n";
				final << comment << "\t{\r\n";
				final << comment << "\t}\r\n";
			end
		end

		# OpenCLGlobalFuncNames()
		global_funcs=get_global_func func_str_origin
		if(global_funcs.size>0)
			log  "global_funcs:  #{global_funcs}"
			final << "\r\n";
			final << "\tvirtual vector<string> OpenCLGlobalFuncNames() const override\r\n";
			final << "\t{\r\n";

			final << "\t	return vector<string> { "+ global_funcs.join(", ") +" };\r\n";
			final << "\t}\r\n";
		end

		if (result.isparvar)
			# Init()
			final << "\r\n";
			final << "protected:\r\n";
			final << "\tvoid Init()\r\n";
			final << "\t{\r\n";
			final << "\t	string prefix = Prefix() + \"" + result.varstringname + "\" \"_\";\r\n";
			final << "\t	m_Params.clear();\r\n";

			paras.each do  |param|
				param_line= ""
				if(param.type==ParaType::T)
					# m_Params.push_back(ParamWithName<T>(&m_Shift, prefix + "swirl3r_shift", T(0.5)));
					# m_Params.push_back(ParamWithName<T>(&mp_zxy, prefix + "zxy",0,eParamType::INTEGER,0,2));
					# m_Params.push_back(ParamWithName<T>(true,&pc_ex, prefix + "ex"));
					param_line << "\t	m_Params.push_back(ParamWithName<T>(" + (param.isprecalc ? "true, ":"") + "&" + param.name + ","
					param_line << " prefix + \"" + param.short_name + "\" "
					param_line << ", 0 " if ! param.isprecalc
					param_line << "));\r\n"
				elsif (param.type==ParaType::ARRAY)
					# for(int i=0;i<COUNT;++i)
					#	 m_Params.push_back(ParamWithName<T>(true,&pc_rand[i], prefix + "rand"+to_string(i)));
					# T pc_rand[COUNT];

					param_line << "\t	for(int i = 0; i < " + param.arr_length_str + "; ++i)\r\n"
					param_line << "\t\t	m_Params.push_back(ParamWithName<T>(" + (param.isprecalc ? "true, ":"") + "&" + param.name + "[i],"
					param_line << " prefix + \"" + param.short_name + "\" + to_string(i) "
					param_line << ", 0 " if ! param.isprecalc
					param_line << "));\r\n"
				end
				final << param_line
			end
			final << "\t}\r\n";

			# private:
			#  param members

			final << "\r\n";
			final << "private:\r\n";
			paras.each do  |param|
				line = "\t" + param.string.strip
				str=""
				if param.type==ParaType::CONST
					str << "Const. "
				elsif param.type==ParaType::CONST_ARRAY
					str << "Const Array. "
				else
					if param.isprecalc
						str << "Precalc. "
					end
					if param.type==ParaType::ARRAY
						str << "Array. "
					end
				end
				line << "\t\t//" + str if ! str.empty?
				line << "\r\n"
				final << line
			end
		end # end of isparvar

		# end of class
		final << "};\r\n";

		final << "// " + (result.isparvar ? "MAKEPREPOSTPLUGINPARVAR" : "MAKEPREPOSTPLUGINVAR")
		final << "( " + result.varclassname + ", " + result.varstringname + " ) ;\r\n"
		final << "\r\n";
		final << "// POPULATEVARLIST_REGPREPOST(" + result.varclassname + ") ;\r\n"

		#parse completed
		# log final
		log "\n================== opencl func string =========================\n========"
		log res_func
		return final
	end

	module ParaType
	  T = 1
	  ARRAY = 2
	  CONST = 3
	  CONST_ARRAY = 4
	end

	def extract_para(para_str)
		debug "Para lines #{para_str.size}"
		delimiters = [" ", ",", ";", "(", ")","{","}", "double", "float", "int", "unsigned", "long", "short", "char","static","const"]
		reg=Regexp.union(delimiters)
		# word.split(reg)
		using_str=[]
		paras=[]
		para_str.each do |raw_line|
			line=raw_line.strip.gsub(/\s+/," ").strip
			if(line.length>0)
				using_str.push line
				used=false
				line.gsub(/\bT /,"").split(reg).each do |e|
					if !used && !e.empty?  && e.match(/[_a-zA-Z]\w*/)
						p=OpenStruct.new
						p.string=line
						p.name=e
						p.name=e.gsub(/=.*$/,"").strip if(e.include? "=")
						p.used=false
						p.type=ParaType::T
						if(line.match(/(?<=\b)(static|const|int|long|short|char|unsigned)(?=\b)/))
							p.type=ParaType::CONST
						end

						if(e.include? "[")
							if  p.type==ParaType::CONST
								p.type=ParaType::CONST_ARRAY
							else
								p.type=ParaType::ARRAY
							end
							p.name=e.gsub(/\s*\[.*$/,"").strip
						end
						used=true
						paras.push p
					end
				end
			end
		end
		paras.each do  |param|
			param.short_name=param.name
			isprecalc=false
			# T mp_L0;  T pc_L0;  T ca_const1[];
			if matched=param.name[/^(?:mp|pc)_(.*)$/,1]
				param.short_name=matched
			end
			isprecalc=true if param.name.start_with? "pc_"
			param.isprecalc=isprecalc

			if(param.type==ParaType::ARRAY)
				param.arr_length_str="0"
				# puts param.name[/^[^\[]*\[([^\[]+)\]/,0]
				if matched=param.string[/^[^\[]*\[([^\[]+)\]/,1]
					param.arr_length_str = matched.strip
				end
			end
		end

		log "Para using lines #{using_str.size}"
		puts using_str if @options.verbose
		summary "--> Para: #{paras.count}"
		log paras# para_str.join
		return paras
	end

	def conv_func(func_arr,paras)
		log "======  original Func lines #{func_arr.size}  ======"
		temp0 =[]
		prepend=[]
		# final.push +="777"
		min_leading_space=999

		# prepare
		for index in 0..func_arr.size-1
			line=func_arr[index]
			line=tidy_all_space(line)
			line_strip=line.tr(" \t\r\n", '')
			next if(line_strip.empty?)
			leading_spaces,=split_leading(line)
			# puts leading_spaces.length
			min_leading_space = leading_spaces.length if leading_spaces.length < min_leading_space
			# puts ("%-4d:"%index)+leading_spaces+"|"
			temp0.push line
		end

		# remove indent
		for index in 0..temp0.size-1
			line=temp0[index]
			leading_spaces,b=split_leading(line)
			line=leading_spaces.sub(" "*min_leading_space,"") + b
			# line=tab_to_t space_to_tab(" "+ line)
			temp0[index] = line
		end

		log "min_leading_space #{min_leading_space}"
		#  replacement

		# VarType escape start
		debug "-----------------------  VarType escape start  ----------------------------------"
		final=escape_vartype(temp0.join("\n").gsub(/^$[\r\n]/, ''))
		final.gsub!(/#{Regexp.escape(@escape_mark)}(?![ \t]*$)/,"\n"+@escape_mark)
		debug "-----------------------  VarType escape after  ----------------------------"
		temp0=final.split("\n")

		for index in 0..temp0.size-1
			line=temp0[index]

			#fixed replacement
			# templine = line.gsub(/(?<=\t| |^)T(?=\*| )/, "real_t").
			# 			 gsub("helper.In.x", "vIn.x").
			# 			 gsub("helper.In.y", "vIn.y").
			# 			 gsub("helper.In.z", "vIn.z").
			# 			 gsub("helper.Out.x", "vOut.x").
			# 			 gsub("helper.Out.y", "vOut.y").
			# 			 gsub("helper.Out.z", "vOut.z").
			# 			 gsub("Floor<T>", "floor").
			# 			 gsub(/\bClamp\b/, "clamp").
			# 			 gsub("std::abs", "fabs").
			# 			 gsub("m_Weight", "\"<< weight <<\"").
			# 			 gsub("std::", "").
			# 			 gsub("rand.Frand01<T>()", "MwcNext01(mwc)").
			# 			 gsub("outPoint.m_", "outPoint->m_").
			# 			 gsub(/(?<=\b)T\s*\(/, "(real_t)(")
			templine=line
			@@FUNC_REPLACE_LIST.each do |pair|
				next if pair.nil? || pair.empty?
				next if pair[0].nil? || pair[1].nil?
				templine.gsub!(pair[0], pair[1])
			end

			#param replacement
			paras.each do |p|
				if (templine.include?(p.name))
					if(p.type==ParaType::T)
						p.used=true
					 	templine.gsub!(/(?<=\b)#{p.name}(?=\b)/, "\"<< " + p.name + " <<\"");
						#  #{Regexp.escape(p.name)}
					elsif (p.type==ParaType::ARRAY)
						if ! p.used
							# index of array[0] ;
							# const int _pc_rand="<<pc_rand<<";
							prepend.push "const int _#{p.name}_=\"<< #{p.name} <<\";"
						end
						p.used=true
						# pc_rand[  777     =>
						# parVars[_pc_rand_+   777
						templine.gsub!(/(?<=\b)#{p.name}\s*\[/, "parVars[_#{p.name}_+");
					elsif (p.type==ParaType::CONST)
						if ! p.used
							# static const int COUNT=17;  =>
							# const int COUNT="<<COUNT<<";
							string=p.string.gsub(/\bstatic\b/,"").gsub(/\bT\b/,"real_t")
							# string=string.gsub(/\bconst\b/,"") if(string.)
							prepend.push string.strip
						end
						p.used=true
					elsif (p.type==ParaType::CONST_ARRAY)
						if ! p.used
							# const T cc_aaa[10]={1.2,3,4,5};  =>
							# real_t cc_aaa[10]={1.2,3,4,5};
							string=p.string.gsub(/\b(static|const)\b/,"").gsub(/\bT\b/,"real_t")
							prepend.push string.strip
						end
					end
				end
			end
			#end of param replacement

			# log ("%-4d:"%index) + " "+templine
			# log ("%-4d:"%index) + " \""+templine+ "\" \\n"
			temp0[index] = templine
		end
		#end of line loop
		final=prepend.join("\n")+"\n"+temp0.join("\n")
		# final=prepend.join("\n")+"\n"+final
		debug final
		return final,min_leading_space
	end

	def func_to_cl(func)
		# debug "====================  func_to_cl  input  ===================="
		# if @options.debug
		# 	func.each_line.with_index do |line,index|
		# 		debug ("%-4d:"%index) + " \""+line+ "\""
		# 	end
		# end
		result=""
		interrupted=true
		new_head= "ss << "
		if(@options.continuous)
			continue_head= " "*new_head.size
		else
			continue_head= "   << "
		end
		func.each_line do |line|
			line=trim_right(line)
			next if line.empty?
			if(line.include? @escape_mark)
				line= trim_right (line.gsub(@escape_mark,"") )
				next if line.empty?
				if(interrupted==false && result.size>1)
					#add ; to previous line if it is "foo"
					result.chomp!("\n")
					result << ";\n"
				end
				interrupted = true
				result << line+ " \n"
			else
				result << ( interrupted ? new_head : continue_head )
				interrupted=false
				result << "\"\\t\\t"+ tab_to_t(tidy(line)," ") +" \\n\"\n"
			end

		end
		if(interrupted==false && result.size>1)
			result.chomp!("\n")
			result << ";\n"
		end

		@@FUNC_POST_REPLACE_LIST.each do |pair|
			next if pair.nil? || pair.empty?
			next if pair[0].nil? || pair[1].nil?
			result.gsub!(pair[0], pair[1])
		end
		# puts final
		# result.each_line.with_index do |line,index|
		# 	log ("%-4d:"%index) + " "+line
		# end
		return result
	end

	def escape_vartype(func)
		final=""
		remain=func
		debug_result_start(func,"escape_vartype",final.object_id)

		# find vartype string in func
		vartype=func.match(/^.*\b(m_VarType|eVariationType)\b/)
		if(vartype)
			vartype_start=vartype.begin(0)
			# index_start = [vartype_start-1, 0].max
			# final << @escape_mark if vartype_start==0
			# final << @escape_mark
			final << func[0 ..vartype_start].chop
			final << @escape_mark
			remain=func[vartype_start ..-1]
			debug   " ======== in escape_vartype ==== VarType has been found"
			# debug   " final:\n" +final
			# debug   " \n\n remain:\n" +remain
		else
			#type is "if" or "switch", but no vartype found, just return
			debug  " ========  no vartype found"
			debug_result_final(func,"escape_vartype")
			return func
		end

		debug   " ======== in escape_vartype ==== VarType has been found"
		# VarType has been found,
		# check its "if" of "switch" or other tings
		if remain.match(/\A.*?\bif\b/)
			# if "if", call parse_if
			debug    "======== in escape_vartype ==== VarType is from 'if' "
			final << parse_type("if",remain)
		elsif remain.match(/\A.*?\bswitch\s*/)
			# elsif "switch", call parse_switch
			debug    "======== in escape_vartype ==== VarType is from 'switch' "
			final << parse_type("switch",remain)
		else
			# else other tings, consume and this line, check  remain
			debug    "======== in escape_vartype ==== VarType is from other things "
			# final << @escape_mark
			final << consume_first_line(remain)
		end
		debug    "======== in escape_vartype ==== at the end of  escape_vartype "
		debug_result_final(final,"escape_vartype")
		return final
	end

	def consume_first_line(func)
		debug  "======== consume_first_line"
		first_line=""
		if(matched=func.match(/\A.*$/))
			first_line = matched[0]
		end
		final=""
		if(first_line.length>0)
			final << first_line
			debug  "consumed :" +  first_line
			# remain = func[first_line.length..-1]
			final << escape_vartype(func[first_line.length..-1])
			return final
		else
			# no line ? ,just return all that has been checked
			warn "extract_first_line: no single line matched"
			return final
		end
		# if(matched=func.match(/\A.*$/))
		# 	return matched
		# else
		# 	return ""
		# end
	end

	def parse_type(type,func)
		# first line of this func maye contains type: "if"  "else" "switch"

		regex=nil
		find_paren=false
		check_else_after=false

		case type
		when "if"
			regex=/(\A.*?\bif\s*)\(/
			find_paren=true
			check_else_after=true
		when "else"
			regex=/(\A\s*)(else\s*)(?=[^\s])/
			find_paren=false
			check_else_after=false
		when "switch"
			regex=/(\A.*?\bswitch\s*)\(/
			find_paren=true
			check_else_after=false
		end

		remain=func
		final=""
		cur=0
		debug_result_start(func,"parse_type: #{type}",final.object_id)

		if matched=remain.match(regex)
			final << @escape_mark
			if(find_paren)
				debug "--------------find_paren"
				lpare=matched.end(1)
				if rpare=find_balanced(remain,lpare)
					debug "--------------rpara found"
					final << remain[cur..rpare]
					cur+=rpare+1
				else
					# rpare not found
					warn "rpare not found"
					final << remain[cur..-1]
					debug_result(final,if_else_switch)
					return final
				end
			else
				debug "--------------else, no need of pare"
				final << matched[1] <<  @escape_mark  << matched[2]
				cur+=matched.end(0);
			end

			#  {  or expression
			# check what's after type head

			final << bracket_or_block(remain[cur..-1] ,type , check_else_after)
		else
			# regex no match, warn, consume this line
			warn 'first line of func does not contain type: "if"  "else" "switch"'
			final << consume_first_line(remain[cur..-1])
		end
		debug_result_final(final,type)
		return final
	end



	def bracket_or_block(func, type , check_else_after)
		# check next is  {   or  xxx;
		#  {  or expression
		# if {
		#    find matching }
		#    extract mid_str
		#		 escape_vartype mid_str
		# switch , need to escape case
		# else   xxx;
		#  consume xxx;
		# end
		remain=func
		final=""
		cur=0

		debug_result_start(func,"bracket_or_block  type:#{type} check_else:#{check_else_after} ",final.object_id)

		if matched=remain[cur..-1].match(/(\A\s*){/)
			#  {
			debug "--------------lbrack found"
			lbrack=cur+matched.end(1)
			final << remain[cur..lbrack]; cur=lbrack+1;
			final << @escape_mark << "\n"
			# puts remain[lbrack..-1]
			if rbrack=find_balanced(remain,lbrack)
				debug "--------------rbrack found:"
				# debug remain[lbrack..rbrack]
				# final << remain[cur..rbrack-1]; ########################maybe recursive
				mid_str=escape_vartype(remain[cur..rbrack-1])
				if(type =="switch")
					#inside switch deal things outside {}
					str=mid_str
					result_switch=""
#useful
					loop do
						if(aaa=str.index("{"))
							bbb=find_balanced(str,aaa)
							switch_lbrack=aaa
							switch_rbrack= (bbb.nil? ? (str.size) : bbb)
						else
							switch_lbrack=switch_rbrack=str.size
						end
						str_before=str[0..switch_lbrack-1]
						str_inside=str[switch_lbrack..switch_rbrack]
						str_after=str[switch_rbrack+1..-1]
						tmp=str_before.gsub(/([ \t]*case\b[^:]*?:|[ \t]*\bbreak[ \t]*;|[ \t]*\bdefault[ \t]*:)/,@escape_mark+'\1')
						# puts " switch       str_before           "
						# puts str_before
						# puts " switch       str_inside           "
						# puts str_inside
						# puts " switch       str_after            "
						# puts str_after
						# puts " switch      ----------------------"
						str=str_after
						result_switch << tmp
						result_switch << str_inside if str_inside
						if str.nil? || str.empty?
							result_switch << str_after if str_after
							break
						end
					end

					mid_str=result_switch
				end
				# in if or else just collect result
				debug "              ----------- checked mid_str: "
				debug mid_str
				final << mid_str
				final << "\n" << @escape_mark << remain[rbrack];
				cur=rbrack+1;
			else
				# rbrack not found, collect all
				final << remain[cur..-1]
				warn "rbrack not found, collect all"
				debug_result(final,if_else_switch)
				return final
			end
		else
			#  expression, collect remains
			debug "              ----------- expression, collect remains"
			final << @escape_mark << "\n";
			# final << remain[cur..-1]
			if matched=remain[cur..-1].match(/\A[^;]*;/)
				final << matched[0];
				cur+=matched.end(0);
				debug "            -----catched"
				debug matched[0]
			else
				warn "no expression return rest"
				final << remain[cur..-1]
				debug_result(final,if_else_switch)
				return final
			end
			# final << check_vartype_if(remain[cur..-1],"if")
			# return final
		end

		# if  check_else
			# if next is else
		#	   parse_type("else",remain)
			# else
		#	  escape_vartype remain
			# end
		# else
		#  escape_vartype remain
		# end
		if check_else_after
			debug "              ----------- check_else_after"
			if matched=remain[cur..-1].match(/\A\s*else\b/)
				debug "              ----------- has else, call parse_type(else)"
				final << parse_type("else",remain[cur..-1])
			else
				debug "              ----------- no else found, call escape_vartype"
				final << escape_vartype(remain[cur..-1])
			end
		else
			debug "              ----------- no need to check else, call escape_vartype"
			final << escape_vartype(remain[cur..-1])
		end
		debug_result_final(final,type)
		return final

	end

	def debug_result_start(func, type, id)
		debug_result(func, type, id , true)
	end

	def debug_result_final(final, type)
		debug_result(final, type, final.object_id , false)
	end

	def debug_result(final, type, id , at_start=false)
		return unless @options.debug
		if(at_start)
			debug "\n    ######################## fuction start ######################## "
			debug "type:   #{type} ( #{id} ) input:\n#{final}"
			debug "                                                  =================== "
		else
			debug "\n    ======================== before return ======================== "
			debug "type:   #{type} ( #{id} ) returns:\n#{final}"
			debug "                                                  ^ ^ ^ ^ ^ ^ ^ ^ ^ ^ "
		end
	end

	def get_global_func(func_str)
		if @global_funcs.empty?
			func_array=@@GLOBAL_FUNC_STRING.split("\n")
			func_array.each do |str|
				s=str.strip
				@global_funcs << /\b#{s}\b/ if ! s.empty?
			end
			debug "global_funcs counts: #{@global_funcs.size}"
		end
		result=[]
		# result<< '"test"'
		@global_funcs.each do |func_regex|
			if matched=func_str.match( func_regex)
				result << ('"' + matched[0] + '"')
			end
		end
		return result
	end

	def find_balanced(string,pos )
	  return nil if string.nil? || string.empty?
	  pairs="{}[]()"
	  pos=pos%string.size
	  needle=string[pos]
	  return nil if needle.nil? || needle.length !=1 || !pairs.include?(needle)

	  index=pairs.index(needle)
	  noreverse=index.even?
	  pair=pairs[index+ (noreverse ? 1 : -1 )]
	  return nil if pair.nil? || pair.empty?

	  substr=noreverse ? string[pos+1..-1] : string[0..pos-1].reverse
	  # debug pair +"  for  "+ substr
	  depth = 1
	  found=nil
	  substr.each_char.with_index do |char,i|

	    if char==needle
	      depth+=1
	    elsif char==pair
	      depth-=1
	      (found=i ; break) if depth==0
	    end
	    # puts "#{i} : #{char}    depth: #{depth}"
	  end
	  return pos+(noreverse ? 1+found : -1-found) if found
	  return nil
	end


end

@@GLOBAL_FUNC_STRING=<<-GLOBALFUNC
LRint
Round
Fract
HashShadertoy
Sign
SignNz
Sqr
SafeSqrt
SafeDivInv
Cube
Hypot
Spread
Powq4
Powq4c
Zeps
Lerp
Fabsmod
Fosc
Foscn
LogScale
LogMap
ClampGte
Swap
Modulate
RealDivComplex
ComplexDivComplex
ComplexMultReal
ComplexMultComplex
ComplexPlusReal
ComplexPlusComplex
ComplexMinusReal
ComplexMinusComplex
ComplexSqrt
ComplexLog
ComplexExp
Hash
Vratio
Closest
Voronoi
SimplexNoise3D
PerlinNoise3D
EvalRational
J1
JacobiElliptic
NOISE_INDEX
NOISE_POINTS
OFFSETS
P1
Q1
P2
Q2
PC
QC
PS
QS
GLOBALFUNC

conv=ConvOpenCL.new
conv.process ARGV

exit



























#
