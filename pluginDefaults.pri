VERSION = 1.0.0.19
win32:CONFIG += skip_target_version_ext
CONFIG += c++14


EMBER_PLUGIN_ROOT = $$absolute_path(./)

SRC_DIR = $$EMBER_PLUGIN_ROOT/Source

INCLUDE_DIR = $$EMBER_PLUGIN_ROOT/Include
INCLUDEPATH += $$absolute_path($$INCLUDE_DIR)
INCLUDEPATH += $$absolute_path($$INCLUDE_DIR/Ember)

EXTERNAL_LIB = $$absolute_path($$EMBER_PLUGIN_ROOT/Deps)
LIBS = ""
LIBS += -L$$absolute_path($$EXTERNAL_LIB) -lember

#message(PWD: $$absolute_path($$PWD))
#message(EMBER_PLUGIN_ROOT: $$EMBER_PLUGIN_ROOT )
#message(INCLUDEPATH: $$INCLUDEPATH)
#message(EXTERNAL_LIB: $$EXTERNAL_LIB)

#Declare output paths for each configuration.
CONFIG(release, debug|release) {
	CONFIG += warn_off
	DESTDIR = $$absolute_path($$EMBER_PLUGIN_ROOT/Bin/release)
}

CONFIG(debug, debug|release) {
	DESTDIR = $$absolute_path($$EMBER_PLUGIN_ROOT/Bin/debug)
}

#Set compiler options.
QMAKE_CXXFLAGS_RELEASE += -O2
QMAKE_CXXFLAGS_RELEASE += -DNDEBUG
QMAKE_CXXFLAGS += -D_M_X64
QMAKE_CXXFLAGS += -D_CONSOLE
QMAKE_CXXFLAGS += -D_USRDLL

win32 {
	DEFINES += _WIN32
	DEFINES -= _UNICODE UNICODE
	QMAKE_CXXFLAGS += -bigobj #Allow for very large object files.
	QMAKE_CXXFLAGS += /MP #Enable multi-processor compilation.
	QMAKE_CXXFLAGS += /Zc:wchar_t #Treat wchar_t as builtin type (we don't use wchar_t anyway).
	QMAKE_CXXFLAGS += /Zi #Debug information format: program database.
	QMAKE_CXXFLAGS += /Gm- #Disable minimal rebuild, needed to allow /MP.
	QMAKE_CXXFLAGS += /fp:precise #Precise floating point model.
	QMAKE_CXXFLAGS += /fp:except- #Disable floating point exceptions.
	QMAKE_CXXFLAGS += /D "WIN32"
	QMAKE_CXXFLAGS += /D "_WINDOWS"
	QMAKE_CXXFLAGS += /D "_USRDLL"
	QMAKE_CXXFLAGS += /D "_WINDLL" #Build as a DLL.
	QMAKE_CXXFLAGS += /D "_MBCS" #Use multi-byte character set.
	QMAKE_CXXFLAGS += /errorReport:prompt #Internal compiler error reporting, prompt immediately.
	QMAKE_CXXFLAGS += /GF #Enable string pooling.
	QMAKE_CXXFLAGS += /WX- #Don't treat warnings as errors.
	QMAKE_CXXFLAGS += /Zc:forScope #Force conformance in for loop scope.
	QMAKE_CXXFLAGS += /Gd #Calling convention: __cdecl.
	QMAKE_CXXFLAGS += /EHsc #Enable C++ exceptions.
	QMAKE_CXXFLAGS += /nologo #Suppress compiler startup banner.

	QMAKE_CXXFLAGS_RELEASE += /GS- #Disable security check.
	QMAKE_CXXFLAGS_RELEASE += /MD #Link to multi-threaded DLL.
	QMAKE_CXXFLAGS_RELEASE += /Gy #Enable function level linking.
	QMAKE_CXXFLAGS_RELEASE += /O2 #Maximize speed.
	QMAKE_CXXFLAGS_RELEASE += /Ot #Favor fast code.
	QMAKE_CXXFLAGS_RELEASE += /D "NDEBUG" #Release mode.

	QMAKE_CXXFLAGS_DEBUG += /W3 #Error warning level to 3.
	QMAKE_CXXFLAGS_DEBUG += /GS #Enable security check.
	QMAKE_CXXFLAGS_DEBUG += /MDd #Link to multi-threaded debug DLL.
	QMAKE_CXXFLAGS_DEBUG += /Od #Optimization disabled.
	QMAKE_CXXFLAGS_DEBUG += /D "_DEBUG" #Debug mode.
	QMAKE_CXXFLAGS_DEBUG += /RTC1 #Basic runtime checks: stack frames and uninitialized variables.
    QMAKE_CXXFLAGS_DEBUG += /Ob2 #Inline function expansion: any suitable.
}

!win32 {
	native {
		QMAKE_CXXFLAGS += -march=native
	} else {
		QMAKE_CXXFLAGS += -march=k8
	}

	CMAKE_CXXFLAGS += -DCL_USE_DEPRECATED_OPENCL_1_1_APIS # Not sure if this is needed. We remove it if all systems we build on support 1.2.
	QMAKE_CXXFLAGS_RELEASE += -fomit-frame-pointer
	QMAKE_CXXFLAGS += -fPIC
	QMAKE_CXXFLAGS += -fpermissive
	QMAKE_CXXFLAGS += -pedantic
	QMAKE_CXXFLAGS += -std=c++14
	QMAKE_CXXFLAGS += -Wnon-virtual-dtor
	QMAKE_CXXFLAGS += -Wshadow
	QMAKE_CXXFLAGS += -Winit-self
	QMAKE_CXXFLAGS += -Wredundant-decls
	QMAKE_CXXFLAGS += -Wcast-align
	QMAKE_CXXFLAGS += -Winline
	QMAKE_CXXFLAGS += -Wunreachable-code
	QMAKE_CXXFLAGS += -Wswitch-enum
	QMAKE_CXXFLAGS += -Wswitch-default
	QMAKE_CXXFLAGS += -Wmain
	QMAKE_CXXFLAGS += -Wfatal-errors
	QMAKE_CXXFLAGS += -Wall -fpermissive
	QMAKE_CXXFLAGS += -Wold-style-cast
	QMAKE_CXXFLAGS += -Wno-unused-parameter
	QMAKE_CXXFLAGS += -Wno-unused-function
	QMAKE_CXXFLAGS += -Wold-style-cast

	QMAKE_CXXFLAGS_DEBUG += -Wmissing-include-dirs
	QMAKE_CXXFLAGS_DEBUG += -Wzero-as-null-pointer-constant
# NOTE: last path will be the first to search. gcc -I and -L appends to the
# beginning of the path list.
}

macx {
        QMAKE_MAC_SDK = macosx10.14
        QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.14
        QMAKE_CXXFLAGS += -mmacosx-version-min=10.9 -arch x86_64
	QMAKE_CXXFLAGS += -stdlib=libc++
}

unix {
	CONFIG += precompile_header
	QMAKE_LFLAGS_RELEASE += -s
}

#Declare !win32 install dirs.
win32 {#For Windows, the install folder is just the output folder.
	LIB_INSTALL_DIR = $$DESTDIR
	BIN_INSTALL_DIR = $$DESTDIR
	SHARE_INSTALL_DIR = $$DESTDIR
	LAUNCHER_INSTALL_DIR = $$DESTDIR
}

!win32 {
	LIB_INSTALL_DIR = /usr/lib
	BIN_INSTALL_DIR = /usr/bin
	SHARE_INSTALL_DIR = /usr/share/fractorium
	LAUNCHER_INSTALL_DIR = /usr/share/applications
}

